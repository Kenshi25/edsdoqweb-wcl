/**
 * ServiziAcaris.java
 * 
 * AVVERTENZA 
 * Questaq classe contiene una serie di metodi di utilita' per facilitare le prime 
 * fasi di sviluppo dell'integrazione con ACTA tramite esposizione ACARIS: 
 * ogni verticale puo' decidere di gestire come meglio crede il recupero delle 
 * informazioni che seguono
 */
package it.csi.edsdoq.edsdoqweb.util;

import org.apache.log4j.Logger;

import it.doqui.acta.acaris.backofficeservice.BackOfficeServicePort;
import it.doqui.acta.acaris.documentservice.DocumentServicePort;
import it.doqui.acta.acaris.managementservice.ManagementServicePort;
import it.doqui.acta.acaris.multifilingservice.MultifilingServicePort;
import it.doqui.acta.acaris.navigationservice.NavigationServicePort;
import it.doqui.acta.acaris.objectservice.ObjectServicePort;
import it.doqui.acta.acaris.officialbookservice.OfficialBookServicePort;
import it.doqui.acta.acaris.relationshipsservice.RelationshipsServicePort;
import it.doqui.acta.acaris.repositoryservice.RepositoryServicePort;
import it.doqui.acta.acaris.smsservice.SMSServicePort;
import it.doqui.acta.acaris.subjectregistryservice.SubjectRegistryServicePort;
import it.doqui.acta.actasrv.client.AcarisServiceClient;
import it.doqui.acta.actasrv.dto.acaris.type.archive.AcarisRepositoryEntryType;
import it.doqui.acta.actasrv.dto.acaris.type.backoffice.ClientApplicationInfo;
import it.doqui.acta.actasrv.dto.acaris.type.backoffice.PrincipalExtResponseType;
import it.doqui.acta.actasrv.dto.acaris.type.common.CodiceFiscaleType;
import it.doqui.acta.actasrv.dto.acaris.type.common.IdAOOType;
import it.doqui.acta.actasrv.dto.acaris.type.common.IdNodoType;
import it.doqui.acta.actasrv.dto.acaris.type.common.IdStrutturaType;
import it.doqui.acta.actasrv.dto.acaris.type.common.ObjectIdType;
import it.doqui.acta.actasrv.dto.acaris.type.common.PrincipalIdType;

public class ServiziAcaris {

	private String server;
	private String context;
	private int port;
	private boolean mtomEnabled;

	private BackOfficeServicePort bkoService;
	private RepositoryServicePort repositoryService;
	private ObjectServicePort objectService;
	private DocumentServicePort documentService;
	private ManagementServicePort managementService;
	private SMSServicePort smsService;
	private NavigationServicePort navigationService;
	private RelationshipsServicePort relationshipsService;
	private MultifilingServicePort multifilingService;
	private OfficialBookServicePort officialBookService;
	private SubjectRegistryServicePort subjectRegistryService;
	
	Logger logger = Logger.getLogger(ServiziAcaris.class);
	
//	private static ServiziAcaris instance;
//
//	public static ServiziAcaris getInstance() {
//		if (instance == null) {
//			instance = new ServiziAcaris(Constants.MTOM_ENABLED);
//		}
//		return instance;
//	}

//	public ServiziAcaris(boolean mtomEnabled) {
//		this("tst-applogic.reteunitaria.piemonte.it", "/actasrv/", 80, mtomEnabled);
//	}

	public ServiziAcaris(String server, String context, int port, boolean mtomEnabled) {
		this.server = server;
		this.context = context;
		this.port = port;
		this.mtomEnabled = mtomEnabled;
	}

	public ObjectIdType getRepositoryId(String repositoryName) {

		ObjectIdType repositoryId = null;
		AcarisRepositoryEntryType[] repEntries = null;

		try {
			repEntries = getRepositoryServicePort().getRepositories();
		} catch (it.doqui.acta.acaris.repositoryservice.AcarisException acEx) {
			if (acEx.getMessage() != null && acEx.getFaultInfo() != null) {
				logger.error("acEx.getMessage(): " + acEx.getMessage());
				logger.error("acEx.getFaultInfo().getErrorCode(): " + acEx.getFaultInfo().getErrorCode());
				logger.error("acEx.getFaultInfo().getPropertyName(): " + acEx.getFaultInfo().getPropertyName());
				logger.error("acEx.getFaultInfo().getObjectId(): " + acEx.getFaultInfo().getObjectId());
				logger.error("acEx.getFaultInfo().getExceptionType(): " + acEx.getFaultInfo().getExceptionType());
				logger.error("acEx.getFaultInfo().getClassName(): " + acEx.getFaultInfo().getClassName());
			} else {
				logger.error(" fatal application exception ", acEx);
			}
		} catch (Exception ex) {
			logger.error("ex.getMessage() " + ex.getMessage(),ex);
		}

		for (AcarisRepositoryEntryType entry : repEntries) {
			if (entry.getRepositoryName() != null && entry.getRepositoryName().startsWith(repositoryName, 0)) {
				repositoryId = entry.getRepositoryId();
				break;
			}
		}

		if (repositoryId == null) {
			throw new IllegalArgumentException("Errore: repository " + repositoryName + " non trovato");
		}
		return repositoryId;
	}

	public PrincipalIdType getPrincipalExtForOggetto(ObjectIdType repId, String cf, long idAoo,	String appKey) throws it.doqui.acta.acaris.backofficeservice.AcarisException {

		IdAOOType aoo = new IdAOOType();
		aoo.setValue(idAoo);

		CodiceFiscaleType codFiscale = new CodiceFiscaleType();
		codFiscale.setValue(cf);

		ClientApplicationInfo cai = new ClientApplicationInfo();
		cai.setAppKey(appKey);

		PrincipalIdType principalId = null;

		PrincipalExtResponseType[] principal = getBackOfficeServicePort().getPrincipalExt(repId, codFiscale, aoo,
				null, null, cai);
		
		principalId = principal[0].getPrincipalId();

		return principalId;
	}
	
	public PrincipalIdType getPrincipalExt(ObjectIdType repId, String cf, long idAoo, long idStruttura, long idNodo,
			String appKey) throws it.doqui.acta.acaris.backofficeservice.AcarisException {

		IdAOOType aoo = new IdAOOType();
		aoo.setValue(idAoo);

		IdStrutturaType struttura = new IdStrutturaType();
		struttura.setValue(idStruttura);

		IdNodoType nodo = new IdNodoType();
		nodo.setValue(idNodo);

		CodiceFiscaleType codFiscale = new CodiceFiscaleType();
		codFiscale.setValue(cf);

		ClientApplicationInfo cai = new ClientApplicationInfo();
		cai.setAppKey(appKey);

		PrincipalIdType principalId = null;

		PrincipalExtResponseType[] principal = getBackOfficeServicePort().getPrincipalExt(repId, codFiscale, aoo,
				struttura, nodo, cai);
		
		principalId = principal[0].getPrincipalId();

		return principalId;
	}

	public BackOfficeServicePort getBackOfficeServicePort()
			throws it.doqui.acta.acaris.backofficeservice.AcarisException {
		if (bkoService == null) {
			bkoService = AcarisServiceClient.getBackofficeServiceAPI(server, context, port);
		}

		return bkoService;
	}

	public ObjectServicePort getObjectServicePort() throws it.doqui.acta.acaris.objectservice.AcarisException {
		if (objectService == null) {
			objectService = AcarisServiceClient.getObjectServiceAPI(server, context, port, isMtomEnabled());
		}

		return objectService;
	}

	public RepositoryServicePort getRepositoryServicePort()
			throws it.doqui.acta.acaris.repositoryservice.AcarisException {
		if (repositoryService == null) {
			repositoryService = AcarisServiceClient.getRepositoryServiceAPI(server, context, port);
		}

		return repositoryService;
	}

	public DocumentServicePort getDocumentServicePort() throws it.doqui.acta.acaris.documentservice.AcarisException {
		if (documentService == null) {
			documentService = AcarisServiceClient.getDocumentServiceAPI(server, context, port, isMtomEnabled());
		}

		return documentService;
	}

	public ManagementServicePort getManagementServicePort()
			throws it.doqui.acta.acaris.managementservice.AcarisException {
		if (managementService == null) {
			managementService = AcarisServiceClient.getManagementServiceAPI(server, context, port);
		}

		return managementService;
	}

	public SMSServicePort getSmsServicePort() throws it.doqui.acta.acaris.smsservice.AcarisException {
		if (smsService == null) {
			smsService = AcarisServiceClient.getSmsServiceAPI(server, context, port);
		}

		return smsService;
	}

	public NavigationServicePort getNavigationServicePort()
			throws it.doqui.acta.acaris.navigationservice.AcarisException {
		if (navigationService == null) {
			navigationService = AcarisServiceClient.getNavigationServiceAPI(server, context, port);
		}

		return navigationService;
	}

	public RelationshipsServicePort getRelationshipsServicePort()
			throws it.doqui.acta.acaris.relationshipsservice.AcarisException {
		if (relationshipsService == null) {
			relationshipsService = AcarisServiceClient.getRelationshipsServiceAPI(server, context, port,
					isMtomEnabled());
		}

		return relationshipsService;
	}

	public MultifilingServicePort getMultifilingServicePort()
			throws it.doqui.acta.acaris.multifilingservice.AcarisException {
		if (multifilingService == null) {
			multifilingService = AcarisServiceClient.getMultifillingServiceAPI(server, context, port);
		}

		return multifilingService;
	}

	public OfficialBookServicePort getOfficialBookServicePort()
			throws it.doqui.acta.acaris.officialbookservice.AcarisException {
		if (officialBookService == null) {
			officialBookService = AcarisServiceClient.getOfficialBookServiceAPI(server, context, port);
		}

		return officialBookService;
	}

	public SubjectRegistryServicePort getSubjectRegistryServicePort()
			throws it.doqui.acta.acaris.subjectregistryservice.AcarisException {
		if (subjectRegistryService == null) {
			subjectRegistryService = AcarisServiceClient.getSubjectRegistryServiceAPI(server, context, port);
		}

		return subjectRegistryService;
	}

	public boolean isMtomEnabled() {
		return mtomEnabled;
	}

	public void setMtomEnabled(boolean mtomEnabled) {
		this.mtomEnabled = mtomEnabled;
	}
}
