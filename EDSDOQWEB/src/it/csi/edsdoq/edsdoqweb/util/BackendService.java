/**
 * 
 */
package it.csi.edsdoq.edsdoqweb.util;

import javax.servlet.http.HttpServletRequest;

import it.csi.edsdoq.edsdoqweb.filter.IrideIdAdapterFilter;
import it.csi.taif.taifweb.dto.common.UserInfo;

/**
 * @author franc
 *
 */
public class BackendService {
	
	public UserInfo getCurrentUser(HttpServletRequest request) {
		return (UserInfo)request.getSession().getAttribute(IrideIdAdapterFilter.USERINFO_SESSIONATTR);
	}
	
	public void localLogout(HttpServletRequest request) {
		request.getSession().invalidate();
	}
 
}
