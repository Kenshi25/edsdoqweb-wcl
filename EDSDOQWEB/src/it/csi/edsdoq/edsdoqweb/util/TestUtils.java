/**
 * TestUtils.java
 * 
 * AVVERTENZA 
 * Questa classe contiene una serie di metodi di utilita' per facilitare le prime 
 * fasi di sviluppo dell'integrazione con ACTA tramite esposizione ACARIS: 
 * ogni verticale puo' decidere di gestire come meglio crede il recupero delle 
 * informazioni che seguono
 */

package it.csi.edsdoq.edsdoqweb.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.activation.DataHandler;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoDiscIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoRecsIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoSmalIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcAualterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcDocDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcEalxIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcEnergiaIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oMeteoricheIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oScarIndustrialiIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oScarPubbFognIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oSfioratoriIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcEventiDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcFidejDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcPostAiaDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcOmIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAmVcoDiscIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAmVcoRecsIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAmVcoSmalIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAualterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcDocDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcDoquiDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcEalxIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcEnergiaIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcH2oScarIndustrialiIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcH2oScarPubbFognIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcH2oSfioratoriIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcIppcEventiDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcIppcFidejDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcIppcPostAiaDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcOmIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.Ech2oMeteoricheDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiModel;
import it.doqui.acta.actasrv.dto.acaris.type.common.AcarisContentStreamType;
import it.doqui.acta.actasrv.dto.acaris.type.common.EnumMimeTypeType;
import it.doqui.acta.actasrv.dto.acaris.type.common.EnumObjectType;
import it.doqui.acta.actasrv.dto.acaris.type.common.EnumPropertyFilter;
import it.doqui.acta.actasrv.dto.acaris.type.common.EnumQueryOperator;
import it.doqui.acta.actasrv.dto.acaris.type.common.ObjectResponseType;
import it.doqui.acta.actasrv.dto.acaris.type.common.PagingResponseType;
import it.doqui.acta.actasrv.dto.acaris.type.common.PropertyFilterType;
import it.doqui.acta.actasrv.dto.acaris.type.common.PropertyType;
import it.doqui.acta.actasrv.dto.acaris.type.common.QueryConditionType;
import it.doqui.acta.actasrv.dto.acaris.type.common.QueryNameType;
import it.doqui.acta.actasrv.dto.acaris.type.common.ValueType;
import it.doqui.acta.actasrv.dto.acaris.type.document.EnumStepErrorAction;

public class TestUtils {

    public static AcarisContentStreamType creaContentStream(String filePath, final String fileName, final String estensioneFile, EnumMimeTypeType mimeType,
        boolean isWS) throws IOException {

        AcarisContentStreamType contentStream = new AcarisContentStreamType();
        contentStream.setFilename(fileName);
        contentStream.setMimeType(mimeType);

        byte[] stream = getBytesFromFile(filePath + fileName);

        if (isWS) {
            final InputStream iS = new ByteArrayInputStream(stream);
            final OutputStream oS = new ByteArrayOutputStream(stream.length);

            javax.activation.DataSource a = new javax.activation.DataSource() {

                public OutputStream getOutputStream() throws IOException {
                    return oS;
                }

                public String getName() {
                    return fileName;
                }

                public InputStream getInputStream() throws IOException {
                    return iS;
                }

                public String getContentType() {
                    return estensioneFile;
                }
            };

            // valorizzare StreamMTOM se servizio invocato via WS SOAP
            contentStream.setStreamMTOM(new DataHandler(a));
        } else {
            // valorizzare Stream se servizio invocato via PAPD
            contentStream.setStream(stream);
        }

        return contentStream;
    }

    public static byte[] getBytesFromFile(String filePath) throws IOException {
        File file = new File(filePath);

        InputStream is = new FileInputStream(file);

        long length = file.length();

        if (length > Integer.MAX_VALUE) {
            System.err.println("File is too large");
        }

        byte[] bytes = new byte[(int) length];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            System.err.println("File is too large" + file.getName());
        }

        is.close();
        return bytes;
    }

    public static it.doqui.acta.actasrv.dto.acaris.type.document.StepErrorAction[] getDatiTestAzioniVerificaFirma() {

        it.doqui.acta.actasrv.dto.acaris.type.document.StepErrorAction[] azioniVerificaFirma = new it.doqui.acta.actasrv.dto.acaris.type.document.StepErrorAction[7];
        for (int i = 0; i < 7; i++) {
            azioniVerificaFirma[i] = new it.doqui.acta.actasrv.dto.acaris.type.document.StepErrorAction();
            azioniVerificaFirma[i].setAction(EnumStepErrorAction.INSERT);
            azioniVerificaFirma[i].setStep(i + 1);
        }

        return azioniVerificaFirma;
    }

    /**********************************************************************************************************
     * visualizzazione risultati
     *********************************************************************************************************/

    public static void stampa(PagingResponseType pagingResponseType) {
        if (pagingResponseType == null) {
            System.out.println("ATTENZIONE: recordset null");
        } else {
            int max = pagingResponseType.getObjectsLength();
            for (int i = 0; i < max; i++) {
                System.out.println("--------------" + (i + 1) + "--------------");
                ObjectResponseType ort = pagingResponseType.getObjects(i);
                for (int j = 0; j < ort.getPropertiesLength(); j++) {
                    PropertyType pt = ort.getProperties(j);
                    System.out.println(pt.getQueryName().getClassName() + "." + pt.getQueryName().getPropertyName() + ": ");
                    for (int k = 0; k < pt.getValue().getContentLength(); k++) {
                        System.out.println("    " + pt.getValue().getContent(k));
                    }
                }
                System.out.println();
            }
        }
    }

    /**********************************************************************************************************
     * utilities
     *********************************************************************************************************/
    public static PropertyFilterType getPropertyFilterNone() {
        return getPropertyFilter(EnumPropertyFilter.NONE, null, null, null);
    }

    public static PropertyFilterType getPropertyFilterAll() {
        return getPropertyFilter(EnumPropertyFilter.ALL, null, null, null);
    }

    public static PropertyFilterType getPropertyFilterList(String[] className, String[] propertyName, PropertyFilterType prevFilter) {
        return getPropertyFilter(EnumPropertyFilter.LIST, className, propertyName, prevFilter);
    }

    private static PropertyFilterType getPropertyFilter(EnumPropertyFilter type, String[] className, String[] propertyName, PropertyFilterType prevFilter) {
        PropertyFilterType filter = null;
        if (type != null) {
            if (type.value().equals(EnumPropertyFilter.LIST.value())) {
                filter = (prevFilter != null) ? prevFilter : new PropertyFilterType();
                filter.setFilterType(type);
                List<QueryNameType> properties = new ArrayList<QueryNameType>();
                QueryNameType property = null;
                if (className.length == propertyName.length) {
                    if (prevFilter != null && prevFilter.getFilterType().value().equals(EnumPropertyFilter.LIST.value())
                        && prevFilter.getPropertyListLength() > 0) {
                        for (int j = 0; j < prevFilter.getPropertyListLength(); j++) {
                            properties.add(prevFilter.getPropertyList(j));
                        }
                    }
                    for (int i = 0; i < propertyName.length; i++) {
                        property = new QueryNameType();
                        property.setClassName(className[i]);
                        property.setPropertyName(propertyName[i]);
                        properties.add(property);
                    }
                    filter.setPropertyList(properties.toArray(new QueryNameType[0]));
                } else
                    return null;

            } else {
                filter = new PropertyFilterType();
                filter.setFilterType(type);
            }
        }
        return filter;
    }

    public static QueryConditionType[] getCriteria(EnumQueryOperator[] operator, String[] propertyName, String[] value) {
        QueryConditionType[] criteria = null;
        if ((operator != null && operator.length > 0) && (propertyName != null && propertyName.length > 0) && (value != null && value.length > 0)
            && (operator.length == propertyName.length && operator.length == value.length)) {
            List<QueryConditionType> criteri = new ArrayList<QueryConditionType>();
            QueryConditionType criterio = null;
            for (int i = 0; i < propertyName.length; i++) {
                criterio = new QueryConditionType();
                criterio.setOperator(operator[i]);
                criterio.setPropertyName(propertyName[i]);
                criterio.setValue(value[i]);
                criteri.add(criterio);
            }
            criteria = criteri.toArray(new QueryConditionType[0]);
        }
        return criteria;
    }
    
    public static PropertyType buildPropertyType(EnumObjectType tipoOggetto, String nomeAttributo, Object valore){
        PropertyType propertyType = new PropertyType();
        QueryNameType queryName = new QueryNameType();
        queryName.setClassName(tipoOggetto.value());
        queryName.setPropertyName(nomeAttributo);
        propertyType.setQueryName(queryName);
        ValueType value = new ValueType();
        
        if(valore != null && valore.getClass().isArray()){
            Object[] array = (Object[])valore;
            if(array != null && array.length >0){
                String[] elementi = new String[array.length];
                    for(int j = 0; j < array.length; j++){
                        elementi[j] = (String)array[j];
                    }
                    value.setContent(elementi);
            }
        }else{
            String[] elementi = new String[1];
            elementi[0] = (value == null ? null : String.valueOf(valore));
            value.setContent(elementi);
        }
        propertyType.setValue(value);
        return propertyType;
    }
    
	public static String getCode(String token) {
		String code = token;
		Random ran = new Random();
		
		for(int i=0;i<5;i++) {
			int num = ran.nextInt(10);
			code=code+num;
		}
		
		return code;
	}
	
	public static String getToken(String keyRecord) {
		return keyRecord.substring(0, (keyRecord.length()-5));
	}
	
	public static HashMap<String,Object> getDaoMappa(String token){
		HashMap<String, Object> lista = new HashMap<>();
		
		EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();
		lista.put(Constants.EC_DOQUI,ecDoquiDao);
		
		List<EcDoquiModel> ecDoquiList = ecDoquiDao.findAllByToken(token);
		EcDoquiModel ecDoqui = ecDoquiList.get(0);
		
		String nomeTabella = ecDoqui.getKey2();
		
		switch(nomeTabella){
		case Constants.EC_AUA_ITER:
			EcAualterDao ecAuaIterDao = new EcAualterDaoImpl();
			lista.put(Constants.EC_AUA_ITER, ecAuaIterDao);
			break;
		case Constants.EC_EALX_ITER:
			EcEalxIterDao ecEalxIterDao = new EcEalxIterDaoImpl();
			lista.put(Constants.EC_EALX_ITER,ecEalxIterDao);
			break;
		case Constants.EC_ENERGIA_ITER:
			EcEnergiaIterDao ecEnergia = new EcEnergiaIterDaoImpl();
			lista.put(Constants.EC_ENERGIA_ITER,ecEnergia);
			break;
		case Constants.EC_AM_VCODISCITER:
			EcAmVcoDiscIterDao ecAmVcoDisc2Dao = new EcAmVcoDiscIterDaoImpl();
			lista.put(Constants.EC_AM_VCODISCITER, ecAmVcoDisc2Dao);
			break;
		case Constants.EC_AM_VCORECSITER:
			EcAmVcoRecsIterDao ecAmVcoRecsIterDao = new EcAmVcoRecsIterDaoImpl();
			lista.put(Constants.EC_AM_VCORECSITER, ecAmVcoRecsIterDao);
			break;
		case Constants.EC_AM_VCOSMALITER:
			EcAmVcoSmalIterDao ecAmVcoSmalIterDao = new EcAmVcoSmalIterDaoImpl();
			lista.put(Constants.EC_AM_VCOSMALITER, ecAmVcoSmalIterDao);
			break;
		case Constants.EC_H2O_SCARPUBBFOGNITER:
			EcH2oScarPubbFognIterDao ecH2oScarPubbFognIterDao = new EcH2oScarPubbFognIterDaoImpl();
			lista.put(Constants.EC_H2O_SCARPUBBFOGNITER,ecH2oScarPubbFognIterDao);
			break;
		case Constants.EC_H2O_METEORICHEITER:
			EcH2oMeteoricheIterDao meteoricheDao = new Ech2oMeteoricheDaoImpl();
			lista.put(Constants.EC_H2O_METEORICHEITER,meteoricheDao);
			break;
		case Constants.EC_H2O_SCARINDUSTRIALITER:
			EcH2oScarIndustrialiIterDao scarIndDao = new EcH2oScarIndustrialiIterDaoImpl();
			lista.put(Constants.EC_H2O_SCARINDUSTRIALITER,scarIndDao);
			break;
		case Constants.EC_H2O_SFIORATORITER:
			EcH2oSfioratoriIterDao ecH2oSfioratoriIterDao = new EcH2oSfioratoriIterDaoImpl();
			lista.put(Constants.EC_H2O_SFIORATORITER, ecH2oSfioratoriIterDao);
			break;
		case Constants.EC_IPPC_EVENTI:
			EcIppcEventiDao ecIppcEventiDao = new EcIppcEventiDaoImpl();
			lista.put(Constants.EC_IPPC_EVENTI,ecIppcEventiDao);
			break;
		case Constants.EC_IPPC_POSTAIA:
			EcIppcPostAiaDao ecIppcPostAiaDao = new EcIppcPostAiaDaoImpl();
			lista.put(Constants.EC_IPPC_POSTAIA,ecIppcPostAiaDao);
			break;
		case Constants.EC_IPPC_FIDEJ:
			EcIppcFidejDao fidejDao = new EcIppcFidejDaoImpl();
			lista.put(Constants.EC_IPPC_FIDEJ,fidejDao);
			break;
		case Constants.EC_OM_ITER:
			EcOmIterDao omDao = new EcOmIterDaoImpl();
			lista.put(Constants.EC_OM_ITER,omDao);
			break;
		
		}
		return lista;
	}
	
	public static HashMap<Integer,Object> getDaoDocMappa(int numDoc){
		HashMap<Integer, Object> lista = new HashMap<>();
		
		EcDoquiDao doquiDao = new EcDoquiDaoImpl();
		lista.put(0, doquiDao);
		
		for(int i=1;i<=numDoc;i++) {
			EcDocDao dao = new EcDocDaoImpl();
			lista.put(i,dao);
		}
		
		return lista;
	}
	
}
