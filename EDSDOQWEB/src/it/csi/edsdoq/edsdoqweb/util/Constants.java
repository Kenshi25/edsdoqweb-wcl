package it.csi.edsdoq.edsdoqweb.util;

public class Constants {
	
	public static final String COMPONENT_NAME = "EDSDOQWEB";
	
	public static final String USERINS ="ecodata";
	
	public static final String COD_FISCALE = "NVEDSE80A01L219X";
	public static final String APP_KEY = "-113/-25/-71/45/59/-29/32/-42/11/-5/-83/-38/79/-59/58/74";
	
	public static final String LOGGER_NAME = "edsdoqweb";
	
	public static final boolean MTOM_ENABLED = true;
	public static final boolean MTOM_DISABLED = false;
	
	//Nomi Tabelle di supporto e/o sistema
	public static final String EC_DOC="EC_DOC";
	public static final String EC_DOQUI="EC_DOQUI";
	public static final String EC_DOQUI_NODI="EC_DOQUI_NODI";
	public static final String EC_DOQUI_TEMP="EC_DOQUI_TEMP";
	
	//Nomi Tabelle _ITER
	public static final String EC_AUA_ITER="EC_AUA_ITER";
	public static final String EC_EALX_ITER="EC_EALX_ITER";
	public static final String EC_ENERGIA_ITER="EC_ENERGIA_ITER";
	
	//Nomi Tabelle _AM
	public static final String EC_AM_VCODISCITER="EC_AM_VCODISCITER";
	public static final String EC_AM_VCORECSITER="EC_AM_VCORECSITER";
	public static final String EC_AM_VCOSMALITER="EC_AM_VCOSMALITER";

	//Nomi Tabelle _H20
	public static final String EC_H2O_SCARPUBBFOGNITER="EC_H2O_SCARPUBBFOGNITER";
	public static final String EC_H2O_SFIORATORITER="EC_H2O_SFIORATORITER";
	public static final String EC_H2O_METEORICHEITER="EC_H2O_METEORICHEITER";
	public static final String EC_H2O_SCARINDUSTRIALITER="EC_H2O_SCARINDUSTRIALITER";
	
	//Nomi Tabelle _IPPC
	public static final String EC_IPPC_EVENTI="EC_IPPC_EVENTI";
	public static final String EC_IPPC_POSTAIA="EC_IPPC_POSTAIA";
	public static final String EC_IPPC_FIDEJ="EC_IPPC_FIDEJ";
	
	//Nomi Tabelle _OM
	public static final String EC_OM_ITER="EC_OM_ITER";
	
	//Vital Record
	public static final String VRC_ALTO = "alto";
	
	//Errori
	public static final String ER_SER_E171 = "SER-E171";
	public static final String ER_SER_E167 = "SER-E167";
	public static final String ER_SER_E166 = "SER-E166";
}
