package it.csi.edsdoq.edsdoqweb.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoDiscIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoRecsIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoSmalIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcAualterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcDocDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcEalxIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcEnergiaIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oMeteoricheIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oScarIndustrialiIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oScarPubbFognIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oSfioratoriIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcEventiDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcFidejDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcPostAiaDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcOmIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAmVcoDiscIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAmVcoRecsIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAmVcoSmalIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAualterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcDocDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcDoquiDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcEalxIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcEnergiaIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcH2oScarIndustrialiIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcH2oScarPubbFognIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcH2oSfioratoriIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcIppcEventiDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcIppcFidejDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcIppcPostAiaDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcOmIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.Ech2oMeteoricheDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoDiscIter;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoRecsIter;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoSmalIter;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAuaIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDocModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcEalxIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcEnergiaIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oMeteoricheIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oScarIndustrialiIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oScarPubbFognIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oSfioratoriIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcEventiModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcFidejModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcPostAiaModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcOmIterModel;
import it.doqui.acta.acaris.navigationservice.AcarisException;
import it.doqui.acta.actasrv.dto.acaris.type.archive.DossierPropertiesType;
import it.doqui.acta.actasrv.dto.acaris.type.archive.EnumFolderObjectType;
import it.doqui.acta.actasrv.dto.acaris.type.archive.FascicoloRealeAnnualePropertiesType;
import it.doqui.acta.actasrv.dto.acaris.type.archive.MoveDocumentPropertiesType;
import it.doqui.acta.actasrv.dto.acaris.type.common.AcarisContentStreamType;
import it.doqui.acta.actasrv.dto.acaris.type.common.AnnotazioniPropertiesType;
import it.doqui.acta.actasrv.dto.acaris.type.common.CodiceFiscaleType;
import it.doqui.acta.actasrv.dto.acaris.type.common.EnumObjectType;
import it.doqui.acta.actasrv.dto.acaris.type.common.EnumPropertyFilter;
import it.doqui.acta.actasrv.dto.acaris.type.common.EnumPropertyFilterOperation;
import it.doqui.acta.actasrv.dto.acaris.type.common.EnumQueryOperator;
import it.doqui.acta.actasrv.dto.acaris.type.common.IdAOOType;
import it.doqui.acta.actasrv.dto.acaris.type.common.IdNodoType;
import it.doqui.acta.actasrv.dto.acaris.type.common.IdStrutturaType;
import it.doqui.acta.actasrv.dto.acaris.type.common.IdVitalRecordCodeType;
import it.doqui.acta.actasrv.dto.acaris.type.common.NavigationConditionInfoType;
import it.doqui.acta.actasrv.dto.acaris.type.common.ObjectIdType;
import it.doqui.acta.actasrv.dto.acaris.type.common.ObjectMetadataType;
import it.doqui.acta.actasrv.dto.acaris.type.common.ObjectResponseType;
import it.doqui.acta.actasrv.dto.acaris.type.common.PagingResponseType;
import it.doqui.acta.actasrv.dto.acaris.type.common.PrincipalIdType;
import it.doqui.acta.actasrv.dto.acaris.type.common.PropertyFilterType;
import it.doqui.acta.actasrv.dto.acaris.type.common.PropertyType;
import it.doqui.acta.actasrv.dto.acaris.type.common.QueryConditionType;
import it.doqui.acta.actasrv.dto.acaris.type.common.QueryableObjectType;
import it.doqui.acta.actasrv.dto.acaris.type.management.VitalRecordCodeType;

public class ActaClientSrvUtils {

	Logger logger = Logger.getLogger(Constants.LOGGER_NAME);
	private ServiziAcaris sa;

	public ActaClientSrvUtils() {
	}

	public ActaClientSrvUtils(ServiziAcaris sa) {
		this.sa = sa;
	}

	/******************/
	/** moveDocument **/
	/**
	 * @throws it.doqui.acta.acaris.objectservice.AcarisException
	 ****************/
	public boolean fascicolaDocumento(ObjectIdType repoId, PrincipalIdType prinId, ObjectIdType classificazioneId,
			ObjectIdType targetFolderId) throws AcarisException, it.doqui.acta.acaris.objectservice.AcarisException {
		boolean flag = false;

		ObjectIdType sourceFolderId = new ObjectIdType();

		PropertyFilterType filter = new PropertyFilterType();
		filter.setFilterType(EnumPropertyFilter.ALL);

		ObjectResponseType folderParent = new ObjectResponseType();

		folderParent = sa.getNavigationServicePort().getFolderParent(repoId, classificazioneId, prinId, filter);
		sourceFolderId = folderParent.getObjectId();

		// questa associativeProperties viene posto a null nel caso in cui si
		// deve gestire lo spostamento di un documento a cui non e'
		// legato uno smistamento
		// il risultato e' un taglia/incolla dal fasc temporaneo in cui e' stato
		// classificato il documento creato dalla registrazione in una nuova
		// aggregazione
		MoveDocumentPropertiesType moveDocumentProperties = null;

		ObjectIdType objectId = null;
		try {
			objectId = sa.getObjectServicePort().moveDocument(repoId, prinId, classificazioneId, sourceFolderId,
					targetFolderId, moveDocumentProperties);
		} catch (it.doqui.acta.acaris.objectservice.AcarisException acEx) {
			String errore = acEx.getFaultInfo().getErrorCode();
			if (Constants.ER_SER_E171.equalsIgnoreCase(errore)) {
				logger.info(acEx.getMessage());
				objectId = new ObjectIdType();
			} else if (Constants.ER_SER_E167.equals(errore)) {
				try {
					moveDocumentProperties = new MoveDocumentPropertiesType();

					moveDocumentProperties.setOfflineMoveRequest(true);
					objectId = sa.getObjectServicePort().moveDocument(repoId, prinId, classificazioneId, sourceFolderId,
							targetFolderId, moveDocumentProperties);
				} catch (it.doqui.acta.acaris.objectservice.AcarisException acEx2) {
					errore = acEx2.getFaultInfo().getErrorCode();
					if (Constants.ER_SER_E171.equalsIgnoreCase(errore)) {
						logger.info(acEx2.getMessage());
						objectId = new ObjectIdType();
					}
				}
			} else {
				throw acEx;
			}
		}

		if (null != objectId)
			flag = true;

		return flag;
	}

	/*********************/
	/** createFascicolo **/
	/*********************/
	public ObjectIdType createFascicolo(ObjectIdType repoId, PrincipalIdType prinId, ObjectIdType dossierId,
			String token, long aoo, long struttura, long nodo)
			throws it.doqui.acta.acaris.objectservice.AcarisException,
			it.doqui.acta.acaris.managementservice.AcarisException {

		EcDoquiModel ecDoqui = getEcDoqui(token);
		String codiceDossier = StringUtils.leftPad(ecDoqui.getCodDossier(), 8, '0');

		String oggetto = codiceDossier + "-" + ecDoqui.getTipoProc();

		EnumFolderObjectType typeId = EnumFolderObjectType.FASCICOLO_REALE_ANNUALE_PROPERTIES_TYPE;

		// utilizzare la property corrispondente al typeId utilizzato
		FascicoloRealeAnnualePropertiesType fascicoloPropertyType = new FascicoloRealeAnnualePropertiesType();

		fascicoloPropertyType.setOggetto(oggetto);
		fascicoloPropertyType.setDescrizione(ecDoqui.getTipoProc());
		fascicoloPropertyType.setConservazioneCorrente(5);
		fascicoloPropertyType.setConservazioneGenerale(99);

		fascicoloPropertyType.setDatiPersonali(true);
		fascicoloPropertyType.setDatiRiservati(false);
		fascicoloPropertyType.setDatiSensibili(false);

		// valore indicato in sede di analisi archivistica
		// recuperare tramite servizio managementService.getVitalRecordCode
		String vrcDescrizione = Constants.VRC_ALTO;
		VitalRecordCodeType[] vrcArray = {};
		IdVitalRecordCodeType idVitalRecord = new IdVitalRecordCodeType();
		int numErr = 0;
		do {
			try {
				vrcArray = sa.getManagementServicePort().getVitalRecordCode(repoId);

				for (VitalRecordCodeType vrc : vrcArray) {
					if (vrcDescrizione.equalsIgnoreCase(vrc.getDescrizione())) {
						System.out.println("Trovato vital record code " + vrcDescrizione + " con id "
								+ vrc.getIdVitalRecordCode().getValue());
						idVitalRecord = vrc.getIdVitalRecordCode();
						break;
					}
				}
			} catch (Exception e) {
				if (numErr < 10)
					numErr++;
				else
					throw e;
			}
		} while (numErr <= 10 && idVitalRecord.getValue() == 0);

		fascicoloPropertyType.setIdVitalRecordCode(idVitalRecord);

		IdAOOType idAoo = new IdAOOType();
		idAoo.setValue(aoo);
		fascicoloPropertyType.setIdAOORespMat(idAoo);

		IdStrutturaType idStruttura = new IdStrutturaType();
		idStruttura.setValue(struttura);
		fascicoloPropertyType.setIdStrutturaRespMat(idStruttura);

		IdNodoType idNodo = new IdNodoType();
		idNodo.setValue(nodo);
		fascicoloPropertyType.setIdNodoRespMat(idNodo);

		fascicoloPropertyType.setArchivioCorrente(true);

		CodiceFiscaleType codiceFiscale = new CodiceFiscaleType();
		codiceFiscale.setValue(ecDoqui.getUtenteApplicativo());
		fascicoloPropertyType.setUtenteCreazione(codiceFiscale);

		ObjectIdType fascicoloId = null;

		fascicoloId = sa.getObjectServicePort().createFolder(repoId, typeId, prinId, fascicoloPropertyType, dossierId);

		if (null != fascicoloId) {
			Date data = new Date(System.currentTimeMillis());
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			AnnotazioniPropertiesType annotazione = new AnnotazioniPropertiesType();
			annotazione.setAnnotazioneFormale(true);
			annotazione.setDescrizione("Fascicolo creato mediante integrazione applicativa da EDS-ENVI e dall'utente "
					+ ecDoqui.getUtenteApplicativo() + " in data " + sdf.format(data));

			sa.getManagementServicePort().addAnnotazioni(repoId, fascicoloId, prinId, annotazione);
		}

		return fascicoloId;
	}

	/*******************/
	/** createDossier **/
	/*******************/
	public ObjectIdType createDossier(ObjectIdType repoId, PrincipalIdType prinId, ObjectIdType serieDossierId,
			String token, long aoo, long struttura, long nodo)
			throws it.doqui.acta.acaris.objectservice.AcarisException,
			it.doqui.acta.acaris.managementservice.AcarisException {

		EcDoquiModel ecDoqui = getEcDoqui(token);

		EnumFolderObjectType typeId = EnumFolderObjectType.DOSSIER_PROPERTIES_TYPE;

		// utilizzare la property corrispondente al typeId utilizzato
		DossierPropertiesType dossierPropertyType = new DossierPropertiesType();

		String codiceDossier = StringUtils.leftPad(ecDoqui.getCodDossier(), 8, '0');
		dossierPropertyType.setCodice(codiceDossier);

		dossierPropertyType
				.setDescrizione(ecDoqui.getRagioneSociale() + "-" + ecDoqui.getIndirizzo() + "-" + ecDoqui.getComune());

		dossierPropertyType.setConservazioneCorrente(5);
		dossierPropertyType.setConservazioneGenerale(99);

		dossierPropertyType.setDatiPersonali(true);
		dossierPropertyType.setDatiRiservati(false);
		dossierPropertyType.setDatiSensibili(false);

		IdAOOType idAoo = new IdAOOType();
		idAoo.setValue(270);
		dossierPropertyType.setIdAOORespMat(idAoo);

		IdStrutturaType idStruttura = new IdStrutturaType();
		idStruttura.setValue(1110);
		dossierPropertyType.setIdStrutturaRespMat(idStruttura);

		IdNodoType idNodo = new IdNodoType();
		idNodo.setValue(1198);
		dossierPropertyType.setIdNodoRespMat(idNodo);

		dossierPropertyType.setCreazioneFascicoli(true);
		dossierPropertyType.setRiclassificazioneFascicoli(false);
		dossierPropertyType.setInserimentoDocumenti(true);
		dossierPropertyType.setAggiuntaOriClassificazioneDocumenti(true);

		CodiceFiscaleType codiceFiscale = new CodiceFiscaleType();
		codiceFiscale.setValue(ecDoqui.getUtenteApplicativo());
		dossierPropertyType.setUtenteCreazione(codiceFiscale);

		ObjectIdType dossierId = null;

		dossierId = sa.getObjectServicePort().createFolder(repoId, typeId, prinId, dossierPropertyType, serieDossierId);

		if (null != dossierId) {
			Date data = new Date(System.currentTimeMillis());
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			AnnotazioniPropertiesType annotazione = new AnnotazioniPropertiesType();
			annotazione.setAnnotazioneFormale(true);
			annotazione.setDescrizione("Dossier creato mediante integrazione applicativa da EDS-ENVI e dall'utente "
					+ ecDoqui.getUtenteApplicativo() + " in data " + sdf.format(data));

			sa.getManagementServicePort().addAnnotazioni(repoId, dossierId, prinId, annotazione);
		}

		return dossierId;
	}

	/********************/
	/** getFascicoloId **/
	/********************/
	public ObjectIdType getFascicoloId(String token, ObjectIdType repoId, PrincipalIdType prinId,
			ObjectIdType dossierId, long aoo, long struttura, long nodo)
			throws it.doqui.acta.acaris.objectservice.AcarisException {

		EcDoquiModel ecDoqui = getEcDoqui(token);
		String codiceDossier = StringUtils.leftPad(ecDoqui.getCodDossier(), 8, '0');

		String oggetto = codiceDossier + "-" + ecDoqui.getTipoProc();

		QueryableObjectType targetFascicolo = new QueryableObjectType();
		targetFascicolo.setObject(EnumObjectType.FASCICOLO_REALE_ANNUALE_PROPERTIES_TYPE.value());

		PropertyFilterType filter = TestUtils.getPropertyFilterAll();

		EnumQueryOperator[] queryOpeartor = { EnumQueryOperator.EQUALS, EnumQueryOperator.EQUALS,
				EnumQueryOperator.EQUALS, EnumQueryOperator.EQUALS, EnumQueryOperator.EQUALS };
		String[] propertyName = { "oggetto", "descrizione" };
		String[] values = { oggetto, ecDoqui.getTipoProc() };

		QueryConditionType[] criteria = TestUtils.getCriteria(queryOpeartor, propertyName, values);

		NavigationConditionInfoType navLimits = new NavigationConditionInfoType();
		navLimits.setParentNodeId(dossierId);
		navLimits.setLimitToChildren(true);

		Integer maxItem = null;
		Integer skipCount = 0;

		PagingResponseType fascicolo = new PagingResponseType();

		fascicolo = sa.getObjectServicePort().query(repoId, prinId, targetFascicolo, filter, criteria, navLimits,
				maxItem, skipCount);

//		logger.info("*************************** Stampa fascicolo");
//		logger.info("Lenght: " + fascicolo.getObjects().length + " | child: " + fascicolo.isHasMoreItems());
//		TestUtils.stampa(fascicolo);
//		logger.info("*************************** Fine stampa fascicolo");

		ObjectResponseType[] fascicoli = fascicolo.getObjects();

		ObjectIdType fascicoloId = null;

		for (ObjectResponseType obj : fascicoli) {
			String idAoo = searchForPropertyFromORT(obj, "idAOORespMat");
			String idStruttura = searchForPropertyFromORT(obj, "idStrutturaRespMat");
			String idNodo = searchForPropertyFromORT(obj, "idNodoRespMat");

			if (String.valueOf(aoo).equals(idAoo) && String.valueOf(struttura).equals(idStruttura)
					&& String.valueOf(nodo).equals(idNodo)) {
				fascicoloId = obj.getObjectId();
				break;
			}
		}

		return fascicoloId;
	}

	/******************/
	/** getDossierId **/
	/******************/
	public ObjectIdType getDossierId(String token, ObjectIdType repoId, PrincipalIdType prinId,
			ObjectIdType serieDossierId) throws it.doqui.acta.acaris.objectservice.AcarisException {

		EcDoquiModel ecDoqui = getEcDoqui(token);

		QueryableObjectType targetDossier = new QueryableObjectType();
		targetDossier.setObject(EnumObjectType.DOSSIER_PROPERTIES_TYPE.value());

		PropertyFilterType filter = TestUtils.getPropertyFilterAll();

//		 numero registrazione e' su 8 cifre
		String codiceDossier = StringUtils.leftPad(ecDoqui.getCodDossier(), 8, '0');

		EnumQueryOperator[] queryOpeartor = { EnumQueryOperator.EQUALS };
		String[] propertyName = { "codice" };
		String[] values = { codiceDossier };

		QueryConditionType[] criteria = TestUtils.getCriteria(queryOpeartor, propertyName, values);

		NavigationConditionInfoType navLimits = new NavigationConditionInfoType();
		navLimits.setParentNodeId(serieDossierId);
		navLimits.setLimitToChildren(true);

		Integer maxItem = null;
		Integer skipCount = 0;

		PagingResponseType dossier = new PagingResponseType();

		dossier = sa.getObjectServicePort().query(repoId, prinId, targetDossier, filter, criteria, navLimits, maxItem,
				skipCount);

//		logger.info("*************************** Stampa dossier");
//		logger.info("Lenght: " + dossier.getObjects().length + " | child: " + dossier.isHasMoreItems());
//		TestUtils.stampa(dossier);
//		logger.info("*************************** Fine stampa dossier");

		ObjectIdType dossierId = null;

		if (null != dossier && dossier.getObjects().length > 0)
			dossierId = dossier.getObjects(0).getObjectId();

		return dossierId;
	}

	/*********************************/
	/** getSerieDossierId=Aziconpos **/
	/*********************************/
	public ObjectIdType getSerieDossierId(ObjectIdType repoId, PrincipalIdType prinId)
			throws it.doqui.acta.acaris.objectservice.AcarisException {

		QueryableObjectType targetSerieDossier = new QueryableObjectType();
		targetSerieDossier.setObject(EnumObjectType.SERIE_DOSSIER_PROPERTIES_TYPE.value());

		PropertyFilterType filter = TestUtils.getPropertyFilterAll();

		EnumQueryOperator[] queryOpeartor = { EnumQueryOperator.EQUALS, EnumQueryOperator.EQUALS };
		String[] propertyName = { "codice", "stato" };
		String[] values = { "AZICONPOS", "2" };

		QueryConditionType[] criteria = TestUtils.getCriteria(queryOpeartor, propertyName, values);

		// navLimits su null perche' non abbiamo un tagerParent da indicare essendo la
		// serieDossier il punto di partenza
		NavigationConditionInfoType navLimits = null;

		Integer maxItem = null;
		Integer skipCount = 0;

		PagingResponseType serieDossier = new PagingResponseType();

		try {
			serieDossier = sa.getObjectServicePort().query(repoId, prinId, targetSerieDossier, filter, criteria,
					navLimits, maxItem, skipCount);
		} catch (it.doqui.acta.acaris.objectservice.AcarisException acEx) {
			throw acEx;
		}

//		logger.info("*************************** Stampa SerieDossier");
//		logger.info("Lenght: " + serieDossier.getObjects().length + " | child: " + serieDossier.isHasMoreItems());
//		TestUtils.stampa(serieDossier);
//		logger.info("*************************** Fine stampa SerieDossier");

		ObjectIdType serieDossierId = null;
		if (null != serieDossier && serieDossier.getObjects().length > 0)
			serieDossierId = serieDossier.getObjects(0).getObjectId();

		return serieDossierId;
	}

	/**************************/
	/** getIdClassificazione **/
	/**************************/
	/*
	 * per ottenere l'identificativo della classificazione a partire da quello del
	 * documento, necessario per invocare il servizio che sposta la classificazione
	 * moveDocument()
	 */
	public ObjectIdType getDocumentoObjectIdFromClassificazioneObjectId(ObjectIdType repositoryId,
			PrincipalIdType principalId, ObjectIdType classificazioneObjectId) throws AcarisException {
		PagingResponseType result = sa.getNavigationServicePort().getChildren(repositoryId, classificazioneObjectId,
				principalId, TestUtils.getPropertyFilterAll(), new Integer(20), null);

		TestUtils.stampa(result);

		ObjectIdType objectIdDocumento = new ObjectIdType();
		objectIdDocumento.setValue(
				searchForProperty(result, EnumObjectType.DOCUMENTO_SEMPLICE_PROPERTIES_TYPE.value(), "objectId"));
		return objectIdDocumento;
	}

	/**************************/
	/** getIdClassificazione **/
	/**************************/
	/*
	 * per ottenere l'identificativo della classificazione a partire da quello del
	 * documento, necessario per invocare il servizio che sposta la classificazione
	 * moveDocument()
	 */
	public List<ObjectIdType> getListaDocumentoObjectIdFromClassificazioneObjectId(ObjectIdType repositoryId,
			PrincipalIdType principalId, ObjectIdType classificazioneObjectId) throws AcarisException {
		PagingResponseType result = sa.getNavigationServicePort().getChildren(repositoryId, classificazioneObjectId,
				principalId, TestUtils.getPropertyFilterAll(), new Integer(20), null);

		TestUtils.stampa(result);

		List<ObjectIdType> lista = new ArrayList<ObjectIdType>();

		int max = result.getObjectsLength();
		for (int i = 0; i < max; i++) {
			System.out.println("--------------" + (i + 1) + "--------------");
			ObjectResponseType ort = result.getObjects(i);
			for (int j = 0; j < ort.getPropertiesLength(); j++) {
				PropertyType pt = ort.getProperties(j);
				if (pt.getQueryName().getClassName().equals(EnumObjectType.DOCUMENTO_SEMPLICE_PROPERTIES_TYPE.value())
						&& pt.getQueryName().getPropertyName().equals("objectId")) {
					ObjectIdType docId = new ObjectIdType();
					docId.setValue(pt.getValue().getContent(0));
					lista.add(docId);
				} else if (pt.getQueryName().getClassName()
						.equals(EnumObjectType.GRUPPO_ALLEGATI_PROPERTIES_TYPE.value())
						&& pt.getQueryName().getPropertyName().equals("objectId")) {
					ObjectIdType allegatiId = new ObjectIdType();
					allegatiId.setValue(pt.getValue().getContent(0));
					List<ObjectIdType> allegatiListId = getListaAllegatiFromClassificazioneObjectId(repositoryId,
							principalId, allegatiId);
					lista.addAll(allegatiListId);
				}
			}
		}

		return lista;
	}

	/**************************/
	/** getIdAllegati **/
	/**************************/
	private List<ObjectIdType> getListaAllegatiFromClassificazioneObjectId(ObjectIdType repositoryId,
			PrincipalIdType principalId, ObjectIdType classificazioneObjectId) throws AcarisException {
		PagingResponseType result = sa.getNavigationServicePort().getChildren(repositoryId, classificazioneObjectId,
				principalId, TestUtils.getPropertyFilterAll(), new Integer(20), null);

		TestUtils.stampa(result);

		List<ObjectIdType> lista = new ArrayList<ObjectIdType>();

		int max = result.getObjectsLength();
		for (int i = 0; i < max; i++) {
			System.out.println("--------------" + (i + 1) + "--------------");
			ObjectResponseType ort = result.getObjects(i);
			ObjectIdType clasAll = ort.getObjectId();
			ActaClientSrvUtils actaCSU = new ActaClientSrvUtils(sa);
//			ObjectIdType docId = actaCSU.getDocumentoObjectIdFromClassificazioneObjectId(repositoryId, principalId,
//					clasAll);
//			lista.add(docId);
			List<ObjectIdType> listaDocId = actaCSU.getListaDocumentoObjectIdFromClassificazioneObjectId(repositoryId,
					principalId, clasAll);
			lista.addAll(listaDocId);
		}
		return lista;
	}

	/****************/
	/** getOggetto **/
	/****************/
	public ObjectIdType getOggetto(ObjectIdType repositoryId, PrincipalIdType principalId, long idAoo, String codice,
			String anno) throws it.doqui.acta.acaris.officialbookservice.AcarisException {

		QueryableObjectType target = new QueryableObjectType();
		target.setObject("ClassificazioniProtocollateView");

		// RACCOMANDATO usare LIST invece di ALL
//		String[] className = new String[] { EnumObjectType.CLASSIFICAZIONE_PROPERTIES_TYPE.value() };
//		String[] propertyName = new String[] { "objectIdClassificazione" };
//		PropertyFilterType filter = TestUtils.getPropertyFilterList(className, propertyName, null);
		PropertyFilterType filter = TestUtils.getPropertyFilterAll();

		QueryConditionType[] criteria = TestUtils.getCriteria(
				new EnumQueryOperator[] { EnumQueryOperator.EQUALS, EnumQueryOperator.EQUALS,
						EnumQueryOperator.EQUALS },
				new String[] { "codice", "anno", "idAooProtocollante" },
				new String[] { codice, anno, String.valueOf(idAoo) });

		NavigationConditionInfoType navigationLimits = new NavigationConditionInfoType();
		navigationLimits.isLimitToChildren();

		// con queste impostazioni la paginazione e' gestita direttamente dal sistema
		Integer maxItems = null;
		Integer skipCount = 0;

		PagingResponseType pagingResponse = new PagingResponseType();

		try {
			pagingResponse = sa.getOfficialBookServicePort().query(repositoryId, principalId, target, filter, criteria,
					null, maxItems, skipCount);
		} catch (it.doqui.acta.acaris.officialbookservice.AcarisException acEx) {
			logger.error(
					"[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]: errore sollevato dal servizio query");
			logger.error("acEx.getMessage(): " + acEx.getMessage());
			logger.error("acEx.getFaultInfo().getErrorCode(): " + acEx.getFaultInfo().getErrorCode());
			logger.error("acEx.getFaultInfo().getPropertyName(): " + acEx.getFaultInfo().getPropertyName());
			logger.error("acEx.getFaultInfo().getObjectId(): " + acEx.getFaultInfo().getObjectId());
			logger.error("acEx.getFaultInfo().getExceptionType(): " + acEx.getFaultInfo().getExceptionType());
			logger.error("acEx.getFaultInfo().getClassName(): " + acEx.getFaultInfo().getClassName());
			logger.error("acEx.getFaultInfo().getTechnicalInfo: " + acEx.getFaultInfo().getTechnicalInfo());
			throw acEx;
		}

		ObjectIdType objectIdClassificazione = null;
		if (pagingResponse != null && pagingResponse.getObjects().length > 0) {
			logger.info(
					"[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]:  pagingResponse.length = "
							+ pagingResponse.getObjects().length);
			logger.info("[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]:  hasMoreItems = "
					+ pagingResponse.isHasMoreItems());
			objectIdClassificazione = new ObjectIdType();

			objectIdClassificazione.setValue(
					searchForProperty(pagingResponse, EnumObjectType.REGISTRAZIONE_PROPERTIES_TYPE.value(), "oggetto"));

		} else {
			logger.info(
					"[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]:  nessun risultato trovato");
		}

		return objectIdClassificazione;
	}

	/****************************************/
	/** getClassificazioneIdFromProtocollo **/
	/****************************************/
	public ObjectIdType ricercaClassificazioneProtocollo(ObjectIdType repositoryId, PrincipalIdType principalId,
			long idAoo, String codice, String anno) throws it.doqui.acta.acaris.officialbookservice.AcarisException {

		QueryableObjectType target = new QueryableObjectType();
		target.setObject("ClassificazioniProtocollateView");

		PropertyFilterType filter = TestUtils.getPropertyFilterAll();

		QueryConditionType[] criteria = TestUtils.getCriteria(
				new EnumQueryOperator[] { EnumQueryOperator.EQUALS, EnumQueryOperator.EQUALS,
						EnumQueryOperator.EQUALS },
				new String[] { "codice", "anno", "idAooProtocollante" },
				new String[] { codice, anno, String.valueOf(idAoo) });

		NavigationConditionInfoType navigationLimits = new NavigationConditionInfoType();
		navigationLimits.isLimitToChildren();

		// con queste impostazioni la paginazione e' gestita direttamente dal sistema
		Integer maxItems = null;
		Integer skipCount = 0;

		PagingResponseType pagingResponse = new PagingResponseType();

		try {
			pagingResponse = sa.getOfficialBookServicePort().query(repositoryId, principalId, target, filter, criteria,
					null, maxItems, skipCount);
		} catch (it.doqui.acta.acaris.officialbookservice.AcarisException acEx) {
			logger.error(
					"[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]: errore sollevato dal servizio query");
			logger.error("acEx.getMessage(): " + acEx.getMessage());
			logger.error("acEx.getFaultInfo().getErrorCode(): " + acEx.getFaultInfo().getErrorCode());
			logger.error("acEx.getFaultInfo().getPropertyName(): " + acEx.getFaultInfo().getPropertyName());
			logger.error("acEx.getFaultInfo().getObjectId(): " + acEx.getFaultInfo().getObjectId());
			logger.error("acEx.getFaultInfo().getExceptionType(): " + acEx.getFaultInfo().getExceptionType());
			logger.error("acEx.getFaultInfo().getClassName(): " + acEx.getFaultInfo().getClassName());
			logger.error("acEx.getFaultInfo().getTechnicalInfo: " + acEx.getFaultInfo().getTechnicalInfo());
			throw acEx;
		}

		ObjectIdType objectIdClassificazione = null;
		if (pagingResponse != null && pagingResponse.getObjects().length > 0) {
			logger.info(
					"[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]:  pagingResponse.length = "
							+ pagingResponse.getObjects().length);
			logger.info("[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]:  hasMoreItems = "
					+ pagingResponse.isHasMoreItems());
			objectIdClassificazione = new ObjectIdType();
			objectIdClassificazione.setValue(searchForProperty(pagingResponse,
					EnumObjectType.CLASSIFICAZIONE_PROPERTIES_TYPE.value(), "objectIdClassificazione"));

		} else {
			logger.info(
					"[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]:  nessun risultato trovato");
		}

		return objectIdClassificazione;
	}

	/**********************************************/
	/** searchForProperty from ObjectResponseType **/
	/**********************************************/
	private String searchForPropertyFromORT(ObjectResponseType ort, String propertyName) {
		String result = "";

		for (int j = 0; j < ort.getPropertiesLength(); j++) {
			PropertyType pt = ort.getProperties(j);
//			System.out.println(pt.getQueryName().getClassName() + "." + pt.getQueryName().getPropertyName() + ": ");
//			for (int k = 0; k < pt.getValue().getContentLength(); k++) {
//				System.out.println("    " + pt.getValue().getContent(k));
//			}

			if (pt.getQueryName().getPropertyName().equals(propertyName)) {
				System.out.println(
						"[RicercaProtocolloEClassificazione::searchForProperty]: Trovata property " + propertyName);
				result = pt.getValue().getContent(0);
				break;
			}
		}

		return result;
	}

	/***********************/
	/** searchForProperty **/
	/***********************/
	private String searchForProperty(PagingResponseType pagingResponseType, String className, String propertyName) {
		String result = null;

		if (pagingResponseType == null) {
			logger.info("[RicercaProtocolloEClassificazione::searchForProperty]: ATTENZIONE: recordset null");
		} else {
			int max = pagingResponseType.getObjectsLength();
			for (int i = 0; i < max; i++) {
				System.out.println("--------------" + (i + 1) + "--------------");
				ObjectResponseType ort = pagingResponseType.getObjects(i);
				for (int j = 0; j < ort.getPropertiesLength(); j++) {
					PropertyType pt = ort.getProperties(j);
					System.out.println(
							pt.getQueryName().getClassName() + "." + pt.getQueryName().getPropertyName() + ": ");
					for (int k = 0; k < pt.getValue().getContentLength(); k++) {
						System.out.println("    " + pt.getValue().getContent(k));
					}

					if (pt.getQueryName().getClassName().equals(className)
							&& pt.getQueryName().getPropertyName().equals(propertyName)) {
						System.out.println("[RicercaProtocolloEClassificazione::searchForProperty]: Trovata property "
								+ propertyName);
						result = pt.getValue().getContent(0);
					}
				}
			}
		}

		return result;
	}

	/*****************************/
	/** printPropertiesByTarget **/
	/*****************************/
	public void printPropertiesByTarget(ObjectIdType repoId, QueryableObjectType target)
			throws it.doqui.acta.acaris.backofficeservice.AcarisException {
		ObjectMetadataType[] objectMetadataType = sa.getBackOfficeServicePort().getQueryableObjectMetadata(repoId,
				target, EnumPropertyFilterOperation.ALL);
		logger.info("************************Elenco properties da usare nel criteria per: " + target.getObject());
		for (ObjectMetadataType omt : objectMetadataType) {
			logger.info("QueryName: " + omt.getQueryName().getPropertyName());
		}
		logger.info("************************Fine elenco");
	}

	/*****************/
	/** getIdVolume **/
	/*****************/
	public String getIdVolume(ObjectResponseType obj) {
		PropertyType[] prop = obj.getProperties();
		String objectIdVolume = null;
		for (PropertyType pr : prop) {
			String risp = "properties: " + pr.getQueryName().getPropertyName() + " - " + pr.getValue().getContent(0);
			logger.info(risp);
			if ("objectId".equals(pr.getQueryName().getPropertyName()) && pr.getValue() != null
					&& pr.getValue().getContentLength() == 1) {
				objectIdVolume = pr.getValue().getContent(0);
			}
		}
		return objectIdVolume;
	}

	/*****************/
	/** updateToken **/
	/*****************/
	public void updateToken(String token) {
		EcDoquiModel ecDoqui = getEcDoqui(token);
		ecDoqui.setValido(1);

		EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();
		ecDoquiDao.merge(ecDoqui);
	}

	private String getDataProtocollo(ObjectIdType repositoryId, PrincipalIdType principalId, List<String> prot,
			long idAoo) throws it.doqui.acta.acaris.officialbookservice.AcarisException {
		String dataProtocollo = "";

		String codice = StringUtils.leftPad(prot.get(0), 8, '0');
		String anno = prot.get(1);

		QueryableObjectType target = new QueryableObjectType();
		target.setObject("ClassificazioniProtocollateView");

		PropertyFilterType filter = TestUtils.getPropertyFilterAll();

		QueryConditionType[] criteria = TestUtils.getCriteria(
				new EnumQueryOperator[] { EnumQueryOperator.EQUALS, EnumQueryOperator.EQUALS,
						EnumQueryOperator.EQUALS },
				new String[] { "codice", "anno", "idAooProtocollante" },
				new String[] { codice, anno, String.valueOf(idAoo) });

		NavigationConditionInfoType navigationLimits = new NavigationConditionInfoType();
		navigationLimits.isLimitToChildren();

		// con queste impostazioni la paginazione e' gestita direttamente dal sistema
		Integer maxItems = null;
		Integer skipCount = 0;

		PagingResponseType pagingResponse = new PagingResponseType();

		try {
			pagingResponse = sa.getOfficialBookServicePort().query(repositoryId, principalId, target, filter, criteria,
					null, maxItems, skipCount);
		} catch (it.doqui.acta.acaris.officialbookservice.AcarisException acEx) {
			logger.error(
					"[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]: errore sollevato dal servizio query");
			logger.error("acEx.getMessage(): " + acEx.getMessage());
			logger.error("acEx.getFaultInfo().getErrorCode(): " + acEx.getFaultInfo().getErrorCode());
			logger.error("acEx.getFaultInfo().getPropertyName(): " + acEx.getFaultInfo().getPropertyName());
			logger.error("acEx.getFaultInfo().getObjectId(): " + acEx.getFaultInfo().getObjectId());
			logger.error("acEx.getFaultInfo().getExceptionType(): " + acEx.getFaultInfo().getExceptionType());
			logger.error("acEx.getFaultInfo().getClassName(): " + acEx.getFaultInfo().getClassName());
			logger.error("acEx.getFaultInfo().getTechnicalInfo: " + acEx.getFaultInfo().getTechnicalInfo());
			throw acEx;
		}

		ObjectIdType objectDataProt = null;
		if (pagingResponse != null && pagingResponse.getObjects().length > 0) {
			logger.info(
					"[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]:  pagingResponse.length = "
							+ pagingResponse.getObjects().length);
			logger.info("[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]:  hasMoreItems = "
					+ pagingResponse.isHasMoreItems());
			objectDataProt = new ObjectIdType();
			objectDataProt.setValue(searchForProperty(pagingResponse,
					EnumObjectType.REGISTRAZIONE_PROPERTIES_TYPE.value(), "dataProtocollo"));
			dataProtocollo = objectDataProt.getValue();
		} else {
			logger.info(
					"[RicercaProtocolloEClassificazione::ricercaClassificazioneProtocollo]:  nessun risultato trovato");
		}

		return dataProtocollo;
	}

	/*******************/
	/** updateTabella **/
	/*******************/
	public void updateTabella(String token, ObjectIdType repositoryId, PrincipalIdType principalId, List<String> prot,
			String oggetto, long idAoo) throws Exception {
		EcDoquiModel ecDoqui = getEcDoqui(token);

		String codice = StringUtils.leftPad(prot.get(0), 8, '0');
		String protocollo = codice + "/" + prot.get(1);
		String dataProtString = getDataProtocollo(repositoryId, principalId, prot, idAoo);
		Date dataProt;
		try {
			dataProt = new SimpleDateFormat("dd/MM/yyyy").parse(dataProtString);
		} catch (Exception e) {
			throw e;
		}

		String nomeTabella = ecDoqui.getKey2();
		Integer idRecord = Integer.valueOf(ecDoqui.getKey3());

		switch (nomeTabella) {
		case Constants.EC_AUA_ITER:
			EcAualterDao ecAuaIterDao = new EcAualterDaoImpl();
			EcAuaIterModel ecAua = (ecAuaIterDao.findByKey(idRecord));
			ecAua.setProt(protocollo);
			ecAua.setDataProt(dataProt);
			ecAua.setOggetto(oggetto);

			ecAuaIterDao.merge(ecAua);
			break;
		case Constants.EC_EALX_ITER:
			EcEalxIterDao ecEalxIterDao = new EcEalxIterDaoImpl();
			EcEalxIterModel ecEalx = ecEalxIterDao.findByKey(idRecord);
			ecEalx.setProt(protocollo);
			ecEalx.setDataProt(dataProt);
			ecEalx.setOggetto(oggetto);

			ecEalxIterDao.merge(ecEalx);
			break;
		case Constants.EC_ENERGIA_ITER:
			EcEnergiaIterDao ecEnergia = new EcEnergiaIterDaoImpl();
			EcEnergiaIterModel ecEnergiaModel = ecEnergia.findByKey(idRecord);
			ecEnergiaModel.setProt(protocollo);
			ecEnergiaModel.setDataProt(dataProt);
			ecEnergiaModel.setOggetto(oggetto);

			ecEnergia.merge(ecEnergiaModel);
			break;
		case Constants.EC_AM_VCODISCITER:
			EcAmVcoDiscIterDao ecAmVcoDisc2Dao = new EcAmVcoDiscIterDaoImpl();
			EcAmVcoDiscIter ecAmVcoDisc2 = ecAmVcoDisc2Dao.findByKey(idRecord);
			ecAmVcoDisc2.setProt(protocollo);
			ecAmVcoDisc2.setDataProt(dataProt);
			ecAmVcoDisc2.setOggetto(oggetto);

			ecAmVcoDisc2Dao.merge(ecAmVcoDisc2);
			break;
		case Constants.EC_AM_VCORECSITER:
			EcAmVcoRecsIterDao ecAmVcoRecsIterDao = new EcAmVcoRecsIterDaoImpl();
			EcAmVcoRecsIter ecAmVcoRecsIter = ecAmVcoRecsIterDao.findByKey(idRecord);
			ecAmVcoRecsIter.setProt(protocollo);
			ecAmVcoRecsIter.setDataProt(dataProt);
			ecAmVcoRecsIter.setOggetto(oggetto);

			ecAmVcoRecsIterDao.merge(ecAmVcoRecsIter);
			break;
		case Constants.EC_AM_VCOSMALITER:
			EcAmVcoSmalIterDao ecAmVcoSmalIterDao = new EcAmVcoSmalIterDaoImpl();
			EcAmVcoSmalIter ecAmVcoSmalIter = ecAmVcoSmalIterDao.findByKey(idRecord);
			ecAmVcoSmalIter.setProt(protocollo);
			ecAmVcoSmalIter.setDataProt(dataProt);
			ecAmVcoSmalIter.setOggetto(oggetto);

			ecAmVcoSmalIterDao.merge(ecAmVcoSmalIter);
			break;
		case Constants.EC_H2O_SCARPUBBFOGNITER:
			EcH2oScarPubbFognIterDao ecH2oScarPubbFognIterDao = new EcH2oScarPubbFognIterDaoImpl();
			EcH2oScarPubbFognIterModel ecH2oScarPubbFognIter = ecH2oScarPubbFognIterDao.findByKey(idRecord);
			ecH2oScarPubbFognIter.setProt(protocollo);
			ecH2oScarPubbFognIter.setDataProt(dataProt);
			ecH2oScarPubbFognIter.setOggetto(oggetto);

			ecH2oScarPubbFognIterDao.merge(ecH2oScarPubbFognIter);
			break;
		case Constants.EC_H2O_METEORICHEITER:
			EcH2oMeteoricheIterDao meteoricheDao = new Ech2oMeteoricheDaoImpl();
			EcH2oMeteoricheIterModel meteoriche = meteoricheDao.findByKey(idRecord);
			meteoriche.setProt(protocollo);
			meteoriche.setDataProt(dataProt);
			meteoriche.setOggetto(oggetto);

			meteoricheDao.merge(meteoriche);
			break;
		case Constants.EC_H2O_SCARINDUSTRIALITER:
			EcH2oScarIndustrialiIterDao scarIndDao = new EcH2oScarIndustrialiIterDaoImpl();
			EcH2oScarIndustrialiIterModel scarInd = scarIndDao.findByKey(idRecord);
			scarInd.setProt(protocollo);
			scarInd.setDataProt(dataProt);
			scarInd.setOggetto(oggetto);

			scarIndDao.merge(scarInd);
			break;
		case Constants.EC_H2O_SFIORATORITER:
			EcH2oSfioratoriIterDao ecH2oSfioratoriIterDao = new EcH2oSfioratoriIterDaoImpl();
			EcH2oSfioratoriIterModel ecH2oSfioratoriIter = ecH2oSfioratoriIterDao.findByKey(idRecord);
			ecH2oSfioratoriIter.setProt(protocollo);
			ecH2oSfioratoriIter.setDataProt(dataProt);
			ecH2oSfioratoriIter.setOggetto(oggetto);

			ecH2oSfioratoriIterDao.merge(ecH2oSfioratoriIter);
			break;
		case Constants.EC_IPPC_EVENTI:
			EcIppcEventiDao ecIppcEventiDao = new EcIppcEventiDaoImpl();
			EcIppcEventiModel ecIppcEventi = ecIppcEventiDao.findByKey(idRecord);
			ecIppcEventi.setProt(protocollo);
			ecIppcEventi.setDataProt(dataProt);
			ecIppcEventi.setOggetto(oggetto);

			ecIppcEventiDao.merge(ecIppcEventi);
			break;
		case Constants.EC_IPPC_POSTAIA:
			EcIppcPostAiaDao ecIppcPostAiaDao = new EcIppcPostAiaDaoImpl();
			EcIppcPostAiaModel ecIppcPostaia = ecIppcPostAiaDao.findByKey(idRecord);
			ecIppcPostaia.setProt(protocollo);
			ecIppcPostaia.setDataProt(dataProt);
			ecIppcPostaia.setOggetto(oggetto);

			ecIppcPostAiaDao.merge(ecIppcPostaia);
			break;
		case Constants.EC_IPPC_FIDEJ:
			EcIppcFidejDao fidejDao = new EcIppcFidejDaoImpl();
			EcIppcFidejModel fidej = fidejDao.findByKey(idRecord);
			fidej.setProt(protocollo);
			fidej.setDataProt(dataProt);
			fidej.setOggetto(oggetto);

			fidejDao.merge(fidej);
			break;
		case Constants.EC_OM_ITER:
			EcOmIterDao omDao = new EcOmIterDaoImpl();
			EcOmIterModel omIter = omDao.findByKey(idRecord);
			omIter.setProt(protocollo);
			omIter.setDataProt(dataProt);
			omIter.setOggetto(oggetto);

			omDao.merge(omIter);
			break;
		}

	}

	/*******************/
	/** updateTabella v2 **/
	/*******************/
	public void updateTabellaV2(String token, ObjectIdType repositoryId, PrincipalIdType principalId, List<String> prot,
			String oggetto, long idAoo, HashMap<String, Object> mappa) throws Exception {

		Object o = mappa.get(Constants.EC_DOQUI);
		EcDoquiDao doquiDao = null;
		if (o instanceof EcDoquiDao) {
			doquiDao = (EcDoquiDao) o;
		}
		EcDoquiModel ecDoqui = getEcDoqui(doquiDao, token);

		String codice = StringUtils.leftPad(prot.get(0), 8, '0');
		String protocollo = codice + "/" + prot.get(1);
		String dataProtString = getDataProtocollo(repositoryId, principalId, prot, idAoo);
		Date dataProt;
		try {
			dataProt = new SimpleDateFormat("dd/MM/yyyy").parse(dataProtString);
		} catch (Exception e) {
			throw e;
		}

		String nomeTabella = ecDoqui.getKey2();
		Integer idRecord = Integer.valueOf(ecDoqui.getKey3());

		switch (nomeTabella) {
		case Constants.EC_AUA_ITER:
			EcAualterDao ecAuaIterDao = null;
			Object oDao = mappa.get(Constants.EC_AUA_ITER);
			if (oDao instanceof EcAualterDao) {
				ecAuaIterDao = (EcAualterDao) oDao;
			}
			EcAuaIterModel ecAua = (ecAuaIterDao.findByKey(idRecord));
			ecAua.setProt(protocollo);
			ecAua.setDataProt(dataProt);
			ecAua.setOggetto(oggetto);

			ecAuaIterDao.merge(ecAua);
			break;
		case Constants.EC_EALX_ITER:
			EcEalxIterDao ecEalxIterDao = null;
			Object oEalx = mappa.get(Constants.EC_EALX_ITER);
			if (oEalx instanceof EcEalxIterDao) {
				ecEalxIterDao = (EcEalxIterDao) oEalx;
			}
			EcEalxIterModel ecEalx = ecEalxIterDao.findByKey(idRecord);
			ecEalx.setProt(protocollo);
			ecEalx.setDataProt(dataProt);
			ecEalx.setOggetto(oggetto);

			ecEalxIterDao.merge(ecEalx);
			break;
		case Constants.EC_ENERGIA_ITER:
			EcEnergiaIterDao ecEnergia = null;
			Object oEner = mappa.get(Constants.EC_ENERGIA_ITER);
			if (oEner instanceof EcEnergiaIterDao) {
				ecEnergia = (EcEnergiaIterDao) oEner;
			}
			EcEnergiaIterModel ecEnergiaModel = ecEnergia.findByKey(idRecord);
			ecEnergiaModel.setProt(protocollo);
			ecEnergiaModel.setDataProt(dataProt);
			ecEnergiaModel.setOggetto(oggetto);

			ecEnergia.merge(ecEnergiaModel);
			break;
		case Constants.EC_AM_VCODISCITER:
			EcAmVcoDiscIterDao ecAmVcoDisc2Dao = null;
			Object oVcoDi = mappa.get(Constants.EC_AM_VCODISCITER);
			if (oVcoDi instanceof EcAmVcoDiscIterDao) {
				ecAmVcoDisc2Dao = (EcAmVcoDiscIterDao) oVcoDi;
			}
			EcAmVcoDiscIter ecAmVcoDisc2 = ecAmVcoDisc2Dao.findByKey(idRecord);
			ecAmVcoDisc2.setProt(protocollo);
			ecAmVcoDisc2.setDataProt(dataProt);
			ecAmVcoDisc2.setOggetto(oggetto);

			ecAmVcoDisc2Dao.merge(ecAmVcoDisc2);
			break;
		case Constants.EC_AM_VCORECSITER:
			EcAmVcoRecsIterDao ecAmVcoRecsIterDao = null;
			Object oVcoRec = mappa.get(Constants.EC_AM_VCORECSITER);
			if (oVcoRec instanceof EcAmVcoRecsIterDao) {
				ecAmVcoRecsIterDao = (EcAmVcoRecsIterDao) oVcoRec;
			}
			EcAmVcoRecsIter ecAmVcoRecsIter = ecAmVcoRecsIterDao.findByKey(idRecord);
			ecAmVcoRecsIter.setProt(protocollo);
			ecAmVcoRecsIter.setDataProt(dataProt);
			ecAmVcoRecsIter.setOggetto(oggetto);

			ecAmVcoRecsIterDao.merge(ecAmVcoRecsIter);
			break;
		case Constants.EC_AM_VCOSMALITER:
			EcAmVcoSmalIterDao ecAmVcoSmalIterDao = null;
			Object oVcoSmal = mappa.get(Constants.EC_AM_VCOSMALITER);
			if (oVcoSmal instanceof EcAmVcoSmalIterDao) {
				ecAmVcoSmalIterDao = (EcAmVcoSmalIterDao) oVcoSmal;
			}
			EcAmVcoSmalIter ecAmVcoSmalIter = ecAmVcoSmalIterDao.findByKey(idRecord);
			ecAmVcoSmalIter.setProt(protocollo);
			ecAmVcoSmalIter.setDataProt(dataProt);
			ecAmVcoSmalIter.setOggetto(oggetto);

			ecAmVcoSmalIterDao.merge(ecAmVcoSmalIter);
			break;
		case Constants.EC_H2O_SCARPUBBFOGNITER:
			EcH2oScarPubbFognIterDao ecH2oScarPubbFognIterDao = null;
			Object oScar = mappa.get(Constants.EC_H2O_SCARPUBBFOGNITER);
			if (oScar instanceof EcH2oScarPubbFognIterDao) {
				ecH2oScarPubbFognIterDao = (EcH2oScarPubbFognIterDao) oScar;
			}
			EcH2oScarPubbFognIterModel ecH2oScarPubbFognIter = ecH2oScarPubbFognIterDao.findByKey(idRecord);
			ecH2oScarPubbFognIter.setProt(protocollo);
			ecH2oScarPubbFognIter.setDataProt(dataProt);
			ecH2oScarPubbFognIter.setOggetto(oggetto);

			ecH2oScarPubbFognIterDao.merge(ecH2oScarPubbFognIter);
			break;
		case Constants.EC_H2O_METEORICHEITER:
			EcH2oMeteoricheIterDao meteoricheDao = null;
			Object oMete = mappa.get(Constants.EC_H2O_METEORICHEITER);
			if (oMete instanceof EcH2oMeteoricheIterDao) {
				meteoricheDao = (EcH2oMeteoricheIterDao) oMete;
			}
			EcH2oMeteoricheIterModel meteoriche = meteoricheDao.findByKey(idRecord);
			meteoriche.setProt(protocollo);
			meteoriche.setDataProt(dataProt);
			meteoriche.setOggetto(oggetto);

			meteoricheDao.merge(meteoriche);
			break;
		case Constants.EC_H2O_SCARINDUSTRIALITER:
			EcH2oScarIndustrialiIterDao scarIndDao = null;
			Object oScarInd = mappa.get(Constants.EC_H2O_SCARINDUSTRIALITER);
			if (oScarInd instanceof EcH2oScarIndustrialiIterDao) {
				scarIndDao = (EcH2oScarIndustrialiIterDao) oScarInd;
			}
			EcH2oScarIndustrialiIterModel scarInd = scarIndDao.findByKey(idRecord);
			scarInd.setProt(protocollo);
			scarInd.setDataProt(dataProt);
			scarInd.setOggetto(oggetto);

			scarIndDao.merge(scarInd);
			break;
		case Constants.EC_H2O_SFIORATORITER:
			EcH2oSfioratoriIterDao ecH2oSfioratoriIterDao = null;
			Object oSfior = mappa.get(Constants.EC_H2O_SFIORATORITER);
			if (oSfior instanceof EcH2oSfioratoriIterDao) {
				ecH2oSfioratoriIterDao = (EcH2oSfioratoriIterDao) oSfior;
			}
			EcH2oSfioratoriIterModel ecH2oSfioratoriIter = ecH2oSfioratoriIterDao.findByKey(idRecord);
			ecH2oSfioratoriIter.setProt(protocollo);
			ecH2oSfioratoriIter.setDataProt(dataProt);
			ecH2oSfioratoriIter.setOggetto(oggetto);

			ecH2oSfioratoriIterDao.merge(ecH2oSfioratoriIter);
			break;
		case Constants.EC_IPPC_EVENTI:
			EcIppcEventiDao ecIppcEventiDao = null;
			Object oEvent = mappa.get(Constants.EC_IPPC_EVENTI);
			if (oEvent instanceof EcIppcEventiDao) {
				ecIppcEventiDao = (EcIppcEventiDao) oEvent;
			}
			EcIppcEventiModel ecIppcEventi = ecIppcEventiDao.findByKey(idRecord);
			ecIppcEventi.setProt(protocollo);
			ecIppcEventi.setDataProt(dataProt);
			ecIppcEventi.setOggetto(oggetto);

			ecIppcEventiDao.merge(ecIppcEventi);
			break;
		case Constants.EC_IPPC_POSTAIA:
			EcIppcPostAiaDao ecIppcPostAiaDao = null;
			Object oPost = mappa.get(Constants.EC_IPPC_POSTAIA);
			if (oPost instanceof EcIppcPostAiaDao) {
				ecIppcPostAiaDao = (EcIppcPostAiaDao) oPost;
			}
			EcIppcPostAiaModel ecIppcPostaia = ecIppcPostAiaDao.findByKey(idRecord);
			ecIppcPostaia.setProt(protocollo);
			ecIppcPostaia.setDataProt(dataProt);
			ecIppcPostaia.setOggetto(oggetto);

			ecIppcPostAiaDao.merge(ecIppcPostaia);
			break;
		case Constants.EC_IPPC_FIDEJ:
			EcIppcFidejDao fidejDao = null;
			Object oFidej = mappa.get(Constants.EC_IPPC_FIDEJ);
			if (oFidej instanceof EcIppcFidejDao) {
				fidejDao = (EcIppcFidejDao) oFidej;
			}
			EcIppcFidejModel fidej = fidejDao.findByKey(idRecord);
			fidej.setProt(protocollo);
			fidej.setDataProt(dataProt);
			fidej.setOggetto(oggetto);

			fidejDao.merge(fidej);
			break;
		case Constants.EC_OM_ITER:
			EcOmIterDao omDao = null;
			Object oIter = mappa.get(Constants.EC_OM_ITER);
			if (oIter instanceof EcOmIterDao) {
				omDao = (EcOmIterDao) oIter;
			}
			EcOmIterModel omIter = omDao.findByKey(idRecord);
			omIter.setProt(protocollo);
			omIter.setDataProt(dataProt);
			omIter.setOggetto(oggetto);

			omDao.merge(omIter);
			break;
		}

	}

	/***********************************/
	/** saveFile nella tabella EC_DOC **/
	/***********************************/
	public void saveFile(String token, AcarisContentStreamType acaStream) {
		EcDocDao ecDocDao = new EcDocDaoImpl();

		EcDoquiModel ecDoqui = getEcDoqui(token);

		List<EcDocModel> ecDocList = ecDocDao.findByKey1AndKey2AndKey3(ecDoqui.getKey1(), ecDoqui.getKey2(),
				ecDoqui.getKey3());

		EcDocModel ecDoc = null;

		for (EcDocModel ecDocProva : ecDocList) {
			if (null != ecDocProva && acaStream.getFilename().equals(ecDocProva.getNomeFile())) {
				ecDoc = ecDocProva;
				break;
			}
		}

		if (null != ecDoc) {
			ecDoc.setUserIns(Constants.USERINS);
			Date date = new Date(System.currentTimeMillis());
			ecDoc.setDataIns(date);
			ecDoc.setDataDoc(date);
			ecDoc.setKey1(ecDoqui.getKey1());
			ecDoc.setKey2(ecDoqui.getKey2());
			ecDoc.setKey3(ecDoqui.getKey3());

			String filename = acaStream.getFilename();
			List<String> listFileType = Arrays.asList(acaStream.getMimeType().value().split("/"));
			String fileType = listFileType.get(1);

			ecDoc.setNomeFile(filename);
			ecDoc.setTipoFile(fileType);
			ecDoc.setDescr(filename);

			byte[] byteArray = null;
			InputStream in;
			try {
				in = acaStream.getStreamMTOM().getInputStream();
				byteArray = convertInputStreamToByteArray(in);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}

			ecDoc.setFdoc(byteArray);

			ecDocDao.merge(ecDoc);
		} else {
			ecDoc = new EcDocModel();
			ecDoc.setUserIns(Constants.USERINS);
			Date date = new Date(System.currentTimeMillis());
			ecDoc.setDataIns(date);
			ecDoc.setDataDoc(date);
			ecDoc.setKey1(ecDoqui.getKey1());
			ecDoc.setKey2(ecDoqui.getKey2());
			ecDoc.setKey3(ecDoqui.getKey3());

			String filename = acaStream.getFilename();
			List<String> listFileType = Arrays.asList(acaStream.getMimeType().value().split("/"));
			String fileType = listFileType.get(1);

			ecDoc.setNomeFile(filename);
			ecDoc.setTipoFile(fileType);
			ecDoc.setDescr(filename);

			byte[] byteArray = null;
			InputStream in;
			try {
				in = acaStream.getStreamMTOM().getInputStream();
				byteArray = convertInputStreamToByteArray(in);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}

			ecDoc.setFdoc(byteArray);

			ecDocDao.merge(ecDoc);

		}

	}

	/*************************************/
	/** saveFile nella tabella EC_DOC V2 **/
	/**
	 * @throws IOException
	 ***********************************/
	public void saveFileV2(String token, AcarisContentStreamType acaStream, HashMap<Integer, Object> mappa, int numDoc)
			throws IOException {
		EcDoquiDao ecDoquiDao = null;

		Object oDoqui = mappa.get(0);
		if (oDoqui instanceof EcDoquiDao) {
			ecDoquiDao = (EcDoquiDao) oDoqui;
		}

		EcDoquiModel ecDoqui = getEcDoqui(ecDoquiDao, token);

		EcDocDao ecDocDao = null;
		Object oDoc = mappa.get(numDoc);
		if (oDoc instanceof EcDocDao) {
			ecDocDao = (EcDocDao) oDoc;
		}

		List<EcDocModel> ecDocList = ecDocDao.findByKey1AndKey2AndKey3(ecDoqui.getKey1(), ecDoqui.getKey2(),
				ecDoqui.getKey3());

		EcDocModel ecDoc = null;

		for (EcDocModel ecDocProva : ecDocList) {
			if (null != ecDocProva && acaStream.getFilename().equals(ecDocProva.getNomeFile())) {
				ecDoc = ecDocProva;
				break;
			}
		}

		if (null != ecDoc) {
			ecDoc.setUserIns(Constants.USERINS);
			Date date = new Date(System.currentTimeMillis());
			ecDoc.setDataIns(date);
			ecDoc.setDataDoc(date);
			ecDoc.setKey1(ecDoqui.getKey1());
			ecDoc.setKey2(ecDoqui.getKey2());
			ecDoc.setKey3(ecDoqui.getKey3());

			String filename = acaStream.getFilename();
			List<String> listFileType = Arrays.asList(acaStream.getMimeType().value().split("/"));
			String fileType = listFileType.get(1);

			ecDoc.setNomeFile(filename);
			ecDoc.setTipoFile(fileType);
			ecDoc.setDescr(filename);

			byte[] byteArray = null;
			InputStream in;
			in = acaStream.getStreamMTOM().getInputStream();
			byteArray = convertInputStreamToByteArray(in);
			ecDoc.setFdoc(byteArray);
			ecDocDao.merge(ecDoc);

		} else {
			ecDoc = new EcDocModel();
			ecDoc.setUserIns(Constants.USERINS);
			Date date = new Date(System.currentTimeMillis());
			ecDoc.setDataIns(date);
			ecDoc.setDataDoc(date);
			ecDoc.setKey1(ecDoqui.getKey1());
			ecDoc.setKey2(ecDoqui.getKey2());
			ecDoc.setKey3(ecDoqui.getKey3());

			String filename = acaStream.getFilename();
			List<String> listFileType = Arrays.asList(acaStream.getMimeType().value().split("/"));
			String fileType = listFileType.get(1);

			ecDoc.setNomeFile(filename);
			ecDoc.setTipoFile(fileType);
			ecDoc.setDescr(filename);

			byte[] byteArray = null;
			InputStream in;
			in = acaStream.getStreamMTOM().getInputStream();
			byteArray = convertInputStreamToByteArray(in);
			ecDoc.setFdoc(byteArray);
			ecDocDao.merge(ecDoc);
		}

	}

	/***************************/
	/** InputStream to byte[] **/
	/***************************/
	private byte[] convertInputStreamToByteArray(InputStream in) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();

		byte[] buffer = new byte[1024];
		int len;
		// read bytes from the input stream and store them in buffer
		while ((len = in.read(buffer)) != -1) {
			// write bytes from the buffer into output stream
			os.write(buffer, 0, len);
		}
		return os.toByteArray();
	}

	public static EcDoquiModel getEcDoqui(String token) {
		EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();

		List<EcDoquiModel> ecDoquiList = ecDoquiDao.findAllByToken(token);
		EcDoquiModel ecDoqui = ecDoquiList.get(0);
		return ecDoqui;
	}

	public static EcDoquiModel getEcDoqui(EcDoquiDao dao, String token) {
//		EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();

		List<EcDoquiModel> ecDoquiList = dao.findAllByToken(token);
		EcDoquiModel ecDoqui = ecDoquiList.get(0);
		return ecDoqui;
	}

}