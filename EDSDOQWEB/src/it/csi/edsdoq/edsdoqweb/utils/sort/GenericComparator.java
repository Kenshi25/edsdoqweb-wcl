/**
 * 
 */
package it.csi.edsdoq.edsdoqweb.utils.sort;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.BaseEntity;



/**
 * @author Admin
 *
 */
public class GenericComparator<T extends BaseEntity> implements Comparator<T>{

	private SortType sortType   = null;  
	private String   methodName = null;  
	     
	   /** 
	    * Construct a GenericComparator according to field and type 
	    * @param sortField field for ordering
	    * @param sortType ascending (ASC) or descending (DESC)
	    */  
	   public GenericComparator(String sortField, SortType sortType) {  
	      this.sortType   = sortType;  
	      this.methodName = ReflectionUtil.buildGetMethodName(sortField);  
	   }  
	  
	  
	   @SuppressWarnings({ "unchecked", "rawtypes" })  
	   @Override  
	   public int compare(T o1, T o2) {  
	      try {  
	         Method method1 = o1.getClass().getMethod(this.methodName,new Class[]{});  
	         Comparable comp1 = (Comparable) method1.invoke(o1, new Object[]{});  
	           
	         Method method2 = o1.getClass().getMethod(this.methodName, new Class[]{});  
	         Comparable comp2 = (Comparable) method2.invoke(o2, new Object[]{});  
	           
	         return comp1.compareTo(comp2) * this.sortType.getIndex();  
	      } catch (Exception e) {  
	         e.printStackTrace();  
	         throw new RuntimeException(e.getMessage());  
	      }  
	   }  
	     
	   /** 
	    * Organizes the List<T> according to the field and the type (ASC or DESC)
	    *  
	    * @param <T> class of objects to be sorted
	    * @param list List<T> to be ordered
	    * @param sortField field for ordering
	    * @param sortType type of ordering (ASC ou DESC) 
	    */  
	      
	   public static <T extends BaseEntity> void sortList(List<T> list, String sortField,   
	         SortType sortType) {  
	      GenericComparator<T> comparator = new GenericComparator<T>(sortField,   
	            sortType);  
	      Collections.sort(list, comparator);  
	   }  
	     
	   /** 
	    * Organise a T[] according with the field and the type (ASC or DESC)) 
	    *  
	    * @param <T> Class of objects to be sorted
	    * @param array  T[] array to be ordained
	    * @param sortField field for ordering
	    * @param sortType type of ordering (ASC ou DESC) 
	    */ 
	   public static <T extends BaseEntity> void sortArray(T[] array, String sortField,   
	         SortType sortType) {  
	      GenericComparator<T> comparator = new GenericComparator<T>(sortField,   
	            sortType);  
	      Arrays.sort(array, comparator);  
	   }  

}
