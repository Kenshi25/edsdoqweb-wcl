package it.csi.edsdoq.edsdoqweb.utils.sort;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;

public class PojoToString {

	public PojoToString() {
		super();
	}

	public String printMethod(Method[] met, Object thisObj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		StringBuilder tmp = new StringBuilder();
		String nameMethod;
		for (int i = 0; i < met.length; i++) {
			nameMethod = met[i].getName();
			if (nameMethod.startsWith("get") || nameMethod.startsWith("is")) {
				Object obj = met[i].invoke(thisObj, new Object[0]);

				if (obj != null) {
					if (obj instanceof java.lang.Integer || obj instanceof java.lang.String || obj instanceof java.lang.Boolean) {
						nameMethod = filterVariable(nameMethod);
						tmp.append("\t\t" + nameMethod + "\t\t [" + String.valueOf(obj) + "]\n");
					} else if (obj instanceof java.util.Date || obj instanceof java.sql.Timestamp || obj instanceof java.sql.Date) {
						nameMethod = filterVariable(nameMethod);
						SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						tmp.append("\t\t" + nameMethod + "\t\t [" + (sd.format((java.util.Date) obj)) + "]\n");
					} else {
						nameMethod = filterVariable(nameMethod);
						tmp.append("\t\t" + nameMethod + "\t\t [" + String.valueOf(obj) + "]\n");
					}
				} else {
					tmp.append("\t\t" + nameMethod + "\t\t [" + String.valueOf(obj) + "] IS NULL \n");
				}
			}
		}

		return tmp.toString();
	}

	public static String filterVariable(String value) {
		String tmp = "";
		if (value.startsWith("get")) {
			tmp = value.substring(3, 4);
			tmp = tmp.toLowerCase();
			value = value.substring(4, value.length());
			tmp += value;
		} else if (value.startsWith("is")) {
			tmp = value.substring(2, 3);
			tmp = tmp.toLowerCase();
			value = value.substring(3, value.length());
			tmp += value;
		}
		return tmp;
	}
}
