package it.csi.edsdoq.edsdoqweb.dto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import it.csi.edsdoq.edsdoqweb.util.Constants;

@Entity
@Table(name=Constants.EC_DOQUI_NODI)
public class EcDoquiNodiModel extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="IDDOQUINODI")
	private Integer idDoquiNodi;
	
	@Column(name="IDDOQUI")
	private Integer idDoqui;
	
	@Column(name="DESCR_AOO")
	private String descrAoo;
	
	@Column(name="COD_AOO")
	private Integer codAoo;
	
	@Column(name="DESCR_STRUTTURA")
	private String descrStruttura;
	
	@Column(name="COD_STRUTTURA")
	private Integer codStruttura;
	
	@Column(name="DESCR_NODO")
	private String descrNodo;
	
	@Column(name="COD_NODO")
	private Integer codNodo;
	
	@Column(name="OPZIONE")
	private Integer opzione;

	public Integer getIdDoquiNodi() {
		return idDoquiNodi;
	}

	public void setIdDoquiNodi(Integer idDoquiNodi) {
		this.idDoquiNodi = idDoquiNodi;
	}

	public Integer getIdDoqui() {
		return idDoqui;
	}

	public void setIdDoqui(Integer idDoqui) {
		this.idDoqui = idDoqui;
	}

	public String getDescrAoo() {
		return descrAoo;
	}

	public void setDescrAoo(String descrAoo) {
		this.descrAoo = descrAoo;
	}

	public Integer getCodAoo() {
		return codAoo;
	}

	public void setCodAoo(Integer codAoo) {
		this.codAoo = codAoo;
	}

	public String getDescrStruttura() {
		return descrStruttura;
	}

	public void setDescrStruttura(String descrStruttura) {
		this.descrStruttura = descrStruttura;
	}

	public Integer getCodStruttura() {
		return codStruttura;
	}

	public void setCodStruttura(Integer codStruttura) {
		this.codStruttura = codStruttura;
	}

	public String getDescrNodo() {
		return descrNodo;
	}

	public void setDescrNodo(String descrNodo) {
		this.descrNodo = descrNodo;
	}

	public Integer getCodNodo() {
		return codNodo;
	}

	public void setCodNodo(Integer codNodo) {
		this.codNodo = codNodo;
	}

	public Integer getOpzione() {
		return opzione;
	}

	public void setOpzione(Integer opzione) {
		this.opzione = opzione;
	}

	@Override
	public String toString() {
		return "EcDoquiNodiModel [idDoquiNodi=" + idDoquiNodi + ", idDoqui=" + idDoqui + ", descrAoo=" + descrAoo
				+ ", codAoo=" + codAoo + ", descrStruttura=" + descrStruttura + ", codStruttura=" + codStruttura
				+ ", descrNodo=" + descrNodo + ", codNodo=" + codNodo + ", opzione=" + opzione + "]";
	}

}
