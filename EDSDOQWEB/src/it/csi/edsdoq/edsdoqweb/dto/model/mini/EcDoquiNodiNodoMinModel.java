package it.csi.edsdoq.edsdoqweb.dto.model.mini;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EcDoquiNodiNodoMinModel {
	
	public EcDoquiNodiNodoMinModel() {}
	
	public EcDoquiNodiNodoMinModel(String descrNodo, Integer codNodo) {
		this.descrNodo=descrNodo;
		this.codNodo=codNodo;
	}
	
	private String descrNodo;
	private Integer codNodo;
	
	public String getDescrNodo() {
		return descrNodo;
	}
	public void setDescrNodo(String descrNodo) {
		this.descrNodo = descrNodo;
	}
	public Integer getCodNodo() {
		return codNodo;
	}
	public void setCodNodo(Integer codNodo) {
		this.codNodo = codNodo;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EcDoquiNodiNodoMinModel other = (EcDoquiNodiNodoMinModel) obj;
		if (codNodo == null) {
			if (other.codNodo != null)
				return false;
		} else if (!codNodo.equals(other.codNodo))
			return false;
		if (descrNodo == null) {
			if (other.descrNodo != null)
				return false;
		} else if (!descrNodo.equals(other.descrNodo))
			return false;
		return true;
	}
	
	
}
