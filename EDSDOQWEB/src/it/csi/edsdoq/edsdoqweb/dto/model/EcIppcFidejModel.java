package it.csi.edsdoq.edsdoqweb.dto.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import it.csi.edsdoq.edsdoqweb.util.Constants;

@Entity
@Table(name=Constants.EC_IPPC_FIDEJ)
public class EcIppcFidejModel extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="IDFIDEJ")
	private Integer idFidej;

	@Column(name="IDITERPROC")
	private Integer idIterProc;
	
	@Column(name="PROT")
	private String prot;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DATAPROT")
	private Date dataProt;

	@Column(name="DIREZIONE")
	private String direzione;
	
	@Column(name="OGGETTO")
	private String oggetto;

	public Integer getIdFidej() {
		return idFidej;
	}

	public void setIdFidej(Integer idFidej) {
		this.idFidej = idFidej;
	}

	public Integer getIdIterProc() {
		return idIterProc;
	}

	public void setIdIterProc(Integer idIterProc) {
		this.idIterProc = idIterProc;
	}

	public void setOggetto(String oggetto) {
		this.oggetto=oggetto;
	}

	public String getOggetto() {
		return oggetto;
	}

	public String getProt() {
		return prot;
	}

	public void setProt(String prot) {
		this.prot = prot;
	}

	public Date getDataProt() {
		return dataProt;
	}

	public void setDataProt(Date dataProt) {
		this.dataProt = dataProt;
	}

	public String getDirezione() {
		return direzione;
	}

	public void setDirezione(String direzione) {
		this.direzione = direzione;
	}

}
