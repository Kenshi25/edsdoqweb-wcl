package it.csi.edsdoq.edsdoqweb.dto.model.mini;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

public class EcDoquiNodiAooMinModel {

	public EcDoquiNodiAooMinModel() {}
	
	public EcDoquiNodiAooMinModel(String descrAoo, Integer codAoo) {
		this.descrAoo=descrAoo;
		this.codAoo=codAoo;
	}
	
	private String descrAoo;
	private Integer codAoo;
	
	public String getDescrAoo() {
		return descrAoo;
	}
	public void setDescrAoo(String descrAoo) {
		this.descrAoo = descrAoo;
	}
	public Integer getCodAoo() {
		return codAoo;
	}
	public void setCodAoo(Integer codAoo) {
		this.codAoo = codAoo;
	}
	@Override
	public String toString() {
		return "Lista [descrAoo=" + descrAoo + ", codAoo=" + codAoo + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EcDoquiNodiAooMinModel other = (EcDoquiNodiAooMinModel) obj;
		if (codAoo == null) {
			if (other.codAoo != null)
				return false;
		} else if (!codAoo.equals(other.codAoo))
			return false;
		if (descrAoo == null) {
			if (other.descrAoo != null)
				return false;
		} else if (!descrAoo.equals(other.descrAoo))
			return false;
		return true;
	}
	
}
