package it.csi.edsdoq.edsdoqweb.dto.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import it.csi.edsdoq.edsdoqweb.util.Constants;

@Entity
@Table(name=Constants.EC_ENERGIA_ITER)
public class EcEnergiaIterModel extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="IDITER")
	private Integer idIter;
	
	@Column(name="IDENERGIA")
	private Integer idEnergia;
	
	@Column(name="DIREZIONE")
	private String direzione;
	
	@Column(name="PROT")
	private String prot;
	
	@Temporal (TemporalType.DATE)
	@Column(name="DATAPROT")
	private Date dataProt;
	
	@Column(name="OGGETTO")
	private String oggetto;

	public Integer getIdIter() {
		return idIter;
	}

	public void setIdIter(Integer idIter) {
		this.idIter = idIter;
	}

	public Integer getIdEnergia() {
		return idEnergia;
	}

	public void setIdEnergia(Integer idEnergia) {
		this.idEnergia = idEnergia;
	}

	public String getDirezione() {
		return direzione;
	}

	public void setDirezione(String direzione) {
		this.direzione = direzione;
	}

	public String getProt() {
		return prot;
	}

	public void setProt(String prot) {
		this.prot = prot;
	}

	public Date getDataProt() {
		return dataProt;
	}

	public void setDataProt(Date dataProt) {
		this.dataProt = dataProt;
	}

	public String getOggetto() {
		return oggetto;
	}

	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

}
