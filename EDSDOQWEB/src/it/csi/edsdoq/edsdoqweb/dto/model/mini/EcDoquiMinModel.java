package it.csi.edsdoq.edsdoqweb.dto.model.mini;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EcDoquiMinModel {
	
	private String codDossier;
	private String codFisc;
	private String tipoProc;
	private String inOut;
	
	public EcDoquiMinModel(String codDossier, String codFisc,String tipoProc,String inOut) {
		this.codDossier=codDossier;
		this.codFisc=codFisc;
		this.tipoProc=tipoProc;
		this.inOut=inOut;
	}
	
	public String getCodDossier() {
		return codDossier;
	}
	public void setCodDossier(String codDossier) {
		this.codDossier = codDossier;
	}
	public String getCodFisc() {
		return codFisc;
	}
	public void setCodFisc(String codFisc) {
		this.codFisc = codFisc;
	}
	public String getTipoProc() {
		return tipoProc;
	}
	public void setTipoProc(String tipoProc) {
		this.tipoProc = tipoProc;
	}
	public String getInOut() {
		return inOut;
	}
	public void setInOut(String inOut) {
		this.inOut = inOut;
	}
	
	@Override
	public String toString() {
		return "EcDoquiVistaModel [codDossier=" + codDossier + ", codFisc=" + codFisc + ", tipoProc=" + tipoProc
				+ ", inOut=" + inOut + "]";
	}
	
}
