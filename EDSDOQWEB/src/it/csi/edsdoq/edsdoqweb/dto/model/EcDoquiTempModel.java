package it.csi.edsdoq.edsdoqweb.dto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import it.csi.edsdoq.edsdoqweb.util.Constants;

@Entity
@Table(name=Constants.EC_DOQUI_TEMP)
public class EcDoquiTempModel {
		
	@Id
	@GeneratedValue
	@Column(name="ID_TEMP")
	private int idTemp;
	
	@Column(name="KEY_RECORD")
	private String keyRecord;
	
	@Column(name="STATUS")
	private int status;
	
	@Column(name="MESSAGGIO")
	private String messaggio;
	
	public String getKeyRecord() {
		return keyRecord;
	}

	public void setKeyRecord(String keyRecord) {
		this.keyRecord = keyRecord;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessaggio() {
		return messaggio;
	}

	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}
	
	public void setIdTtemp(int idTemp) {
		this.idTemp = idTemp;
	}
	
	
	public int getIdTemp() {
		return idTemp;
	}


}