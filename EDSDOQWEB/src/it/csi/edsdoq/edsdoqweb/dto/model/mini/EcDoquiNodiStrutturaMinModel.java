package it.csi.edsdoq.edsdoqweb.dto.model.mini;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EcDoquiNodiStrutturaMinModel {

	// ATTRIBUTI
	private String descrStruttura;
	private Integer codStruttura;

	public EcDoquiNodiStrutturaMinModel() {
	}

	// COSTRUTTORE
	public EcDoquiNodiStrutturaMinModel(String descrStruttura, Integer codStruttura) {

		this.descrStruttura = descrStruttura;
		this.codStruttura = codStruttura;
	}

	// GET E SET
	public String getDescrStruttura() {
		return descrStruttura;
	}

	public void setDescrStruttura(String descrStruttura) {
		this.descrStruttura = descrStruttura;
	}

	public Integer getCodStruttura() {
		return codStruttura;
	}

	public void setCodStruttura(Integer codStruttura) {
		this.codStruttura = codStruttura;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EcDoquiNodiStrutturaMinModel other = (EcDoquiNodiStrutturaMinModel) obj;
		if (codStruttura == null) {
			if (other.codStruttura != null)
				return false;
		} else if (!codStruttura.equals(other.codStruttura))
			return false;
		if (descrStruttura == null) {
			if (other.descrStruttura != null)
				return false;
		} else if (!descrStruttura.equals(other.descrStruttura))
			return false;
		return true;
	}

}
