package it.csi.edsdoq.edsdoqweb.dto.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import it.csi.edsdoq.edsdoqweb.util.Constants;

@Entity
@Table(name=Constants.EC_DOC)
public class EcDocModel extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="IDDOC")
	private Integer idDoc;
	
	@Column(name="KEY1")
	private String key1;
	
	@Column(name="KEY2")
	private String key2;
	
	@Column(name="KEY3")
	private String key3;
	
	@Column(name="NOMEFILE")
	private String nomeFile;
	
	@Column(name="TIPOFILE")
	private String tipoFile;

	@Column(name="DESCR")
	private String descr;
	
	@Temporal (TemporalType.DATE)
	@Column(name="TO_DATADOC")
	private Date dataDoc;
	
	@Column(name="FDOC")
	private byte[] fdoc;
	
	@Column(name="USERINS")
	private String userIns;
	
	@Temporal (TemporalType.DATE)
	@Column(name="DATAINS")
	private Date dataIns;
	
	@Column(name="USERAGG")
	private String userAgg;
	
	@Temporal (TemporalType.DATE)
	@Column(name="DATAAGG")
	private Date dataAgg;
	
	@Column(name="TO_TMPFILEPATH")
	private String tmpFilePath;

	public Integer getIdDoc() {
		return idDoc;
	}

	public void setIdDoc(Integer idDoc) {
		this.idDoc = idDoc;
	}

	public String getKey1() {
		return key1;
	}

	public void setKey1(String key1) {
		this.key1 = key1;
	}

	public String getKey2() {
		return key2;
	}

	public void setKey2(String key2) {
		this.key2 = key2;
	}

	public String getKey3() {
		return key3;
	}

	public void setKey3(String key3) {
		this.key3 = key3;
	}

	public String getNomeFile() {
		return nomeFile;
	}

	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}

	public String getTipoFile() {
		return tipoFile;
	}

	public void setTipoFile(String tipoFile) {
		this.tipoFile = tipoFile;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public Date getDataDoc() {
		return dataDoc;
	}	

	public void setDataDoc(Date dataDoc) {
		this.dataDoc = dataDoc;
	}
	
	public byte[] getFdoc() {
		return fdoc;
	}

	public void setFdoc(byte[] fdoc) {
		this.fdoc = fdoc;
	}
	
	public String getUserIns() {
		return userIns;
	}

	public void setUserIns(String userIns) {
		this.userIns = userIns;
	}

	public Date getDataIns() {
		return dataIns;
	}

	public void setDataIns(Date dataIns) {
		this.dataIns = dataIns;
	}

	public String getUserAgg() {
		return userAgg;
	}

	public void setUserAgg(String userAgg) {
		this.userAgg = userAgg;
	}

	public Date getDataAgg() {
		return dataAgg;
	}

	public void setDataAgg(Date dataAgg) {
		this.dataAgg = dataAgg;
	}

	public String getTmpFilePath() {
		return tmpFilePath;
	}

	public void setTmpFilePath(String tmpFilePath) {
		this.tmpFilePath = tmpFilePath;
	}

	@Override
	public String toString() {
		return "EcDocModel [idDoc=" + idDoc + ", key1=" + key1 + ", key2=" + key2 + ", key3=" + key3 + ", nomeFile="
				+ nomeFile + ", tipoFile=" + tipoFile + ", descr=" + descr + ", dataDoc=" + dataDoc + ", userIns="
				+ userIns + ", dataIns=" + dataIns + ", userAgg=" + userAgg + ", dataAgg=" + dataAgg + ", tmpFilePath="
				+ tmpFilePath + "]";
	}

}
