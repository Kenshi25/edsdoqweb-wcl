package it.csi.edsdoq.edsdoqweb.dto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import it.csi.edsdoq.edsdoqweb.util.Constants;

@Entity
@Table(name=Constants.EC_DOQUI_NODI)
public class EcDoquiNodiAooModel extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="IDDOQUINODI ")
	private Integer idDoquiNodi;
	
	@Column(name="IDDOQUI")
	private Integer idDoqui;
	
	@Column(name="DESCR_AOO")
	private String descrAoo;
	
	@Column(name="COD_AOO")
	private Integer codAoo;
	
	@Column(name="OPZIONE")
	private Integer opzione;

	public Integer getIdDoQuiNodi() {
		return idDoquiNodi;
	}

	public void setIdDoQuiNodi(Integer idDoQuiNodi) {
		this.idDoquiNodi = idDoQuiNodi;
	}

	public Integer getIdDoqui() {
		return idDoqui;
	}

	public void setIdDoqui(Integer iddoqui) {
		this.idDoqui = iddoqui;
	}

	public String getDescrAoo() {
		return descrAoo;
	}

	public void setDescrAoo(String descrAoo) {
		this.descrAoo = descrAoo;
	}

	public Integer getCodAoo() {
		return codAoo;
	}

	public void setCodAoo(Integer codAoo) {
		this.codAoo = codAoo;
	}

	public Integer getOpzione() {
		return opzione;
	}

	public void setOpzione(Integer opzione) {
		this.opzione = opzione;
	}

	@Override
	public String toString() {
		return "EcDoquiNodiAooModel [idDoQuiNodi=" + idDoquiNodi + ", iddoqui=" + idDoqui + ", descrAoo=" + descrAoo
				+ ", codAoo=" + codAoo + ", opzione=" + opzione + "]";
	}
	
	
	
	
	
	
	
	
	
	
	

}
