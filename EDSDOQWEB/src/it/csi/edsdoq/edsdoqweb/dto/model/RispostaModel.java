package it.csi.edsdoq.edsdoqweb.dto.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RispostaModel {

	private int codice;
	private String messaggio;
	
	public RispostaModel() {};
	
	public int getCodice() {
		return codice;
	}
	public void setCodice(int codice) {
		this.codice = codice;
	}
	public String getMessaggio() {
		return messaggio;
	}
	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}
	
	
}