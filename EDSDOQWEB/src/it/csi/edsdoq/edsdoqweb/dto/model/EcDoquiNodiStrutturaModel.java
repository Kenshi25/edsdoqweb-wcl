package it.csi.edsdoq.edsdoqweb.dto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import it.csi.edsdoq.edsdoqweb.util.Constants;


@Entity
@Table(name=Constants.EC_DOQUI_NODI)
public class EcDoquiNodiStrutturaModel extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
		//COLUMN
		@Id
		@Column(name="IDDOQUINODI")
		private Integer idDoquiNodi;
		
		@Column(name="IDDOQUI")
		private Integer idDoqui;
		
		@Column(name="DESCR_STRUTTURA")
		private String descrStruttura;
		
		@Column(name="COD_STRUTTURA")
		private Integer codStruttura;
		
		@Column(name="OPZIONE")
		private Integer opzione;

		
		//GET E SET
		public Integer getIdDoquiNodi() {
			return idDoquiNodi;
		}
		public void setIdDoquiNodi(Integer idDoquiNodi) {
			this.idDoquiNodi = idDoquiNodi;
		}

		public Integer getIdDoqui() {
			return idDoqui;
		}
		public void setIdDoqui(Integer idDoqui) {
			this.idDoqui = idDoqui;
		}

		public String getDescrStruttura() {
			return descrStruttura;
		}
		public void setDescrStruttura(String descrStruttura) {
			this.descrStruttura = descrStruttura;
		}

		public Integer getCodStruttura() {
			return codStruttura;
		}
		public void setCodStruttura(Integer codStruttura) {
			this.codStruttura = codStruttura;
		}
		
		public Integer getOpzione() {
			return opzione;
		}
		public void setOpzione(Integer opzione) {
			this.opzione = opzione;
		}

	
}
