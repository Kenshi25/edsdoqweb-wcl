package it.csi.edsdoq.edsdoqweb.dto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import it.csi.edsdoq.edsdoqweb.util.Constants;

@Entity
@Table(name=Constants.EC_DOQUI_NODI)
public class EcDoquiNodiNodoModel extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="IDDOQUINODI")
	private Integer idDoquiNodi;
	@Column(name="IDDOQUI")
	private Integer idDoqui;
	@Column(name="DESCR_NODO")
	private String descrNodo;
	@Column(name="COD_NODO")
	private Integer codNodo;
	@Column(name="OPZIONE")
	private Integer opzione;
	
	public Integer getOpzione() {
		return opzione;
	}
	public void setOpzione(Integer opzione) {
		this.opzione = opzione;
	}
	public Integer getIdDoquiNodi() {
		return idDoquiNodi;
	}
	public void setIdDoquiNodi(Integer idDoquiNodi) {
		this.idDoquiNodi = idDoquiNodi;
	}
	public Integer getIdDoqui() {
		return idDoqui;
	}
	public void setIdDoqui(Integer idDoqui) {
		this.idDoqui = idDoqui;
	}
	public String getDescrNodo() {
		return descrNodo;
	}
	public void setDescrNodo(String descrNodo) {
		this.descrNodo = descrNodo;
	}
	public Integer getCodNodo() {
		return codNodo;
	}
	public void setCodNodo(Integer codNodo) {
		this.codNodo = codNodo;
	}
	
}
