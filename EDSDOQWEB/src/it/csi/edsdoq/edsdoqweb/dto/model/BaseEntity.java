package it.csi.edsdoq.edsdoqweb.dto.model;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import it.csi.edsdoq.edsdoqweb.utils.sort.PojoToString;



public class BaseEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5662110780055692055L;
	protected HashMap<String, Object> ht = new HashMap<String, Object>();

	

	public String toString() {
		String tmp = "\n" + super.getClass().getCanonicalName() + "\n{";
		Class<?> claz = this.getClass();

		Method[] met = claz.getDeclaredMethods();

		PojoToString p = new PojoToString();
		try {
			tmp += p.printMethod(met, this);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			//log.error(e, e);
		}

		tmp += "}\n";
		return tmp;
	}

	protected String trimValore(String valore) {
		if (valore == null)
			return null;
		return valore.trim();
	}

	public void setValue(String sKey, Object obj) {
		this.ht.put(sKey, obj);
	}

	public Object getValue(String sKey) {
		return this.ht.get(sKey);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ht == null) ? 0 : ht.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseEntity other = (BaseEntity) obj;
		if (ht == null) {
			if (other.ht != null)
				return false;
		} else if (!ht.equals(other.ht))
			return false;
		return true;
	}

	public BaseEntity() {
		super();
	}

}
