package it.csi.edsdoq.edsdoqweb.dto.model.mini;


import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EcDoquiIterMinProt {
	
	public EcDoquiIterMinProt() {}
	public EcDoquiIterMinProt(String prot, Date dataProt,String direzione,String oggetto) {
		this.prot=prot;
		this.dataProt=dataProt;
		this.direzione=direzione;
		this.oggetto=oggetto;
	}

	private String prot;
	private Date dataProt;
	private String direzione;
	private String oggetto;
	
	public String getProt() {
		return prot;
	}

	public void setProt(String prot) {
		this.prot = prot;
	}

	public Date getDataProt() {
		return this.dataProt;
	}

	public void setDataProt(Date dataProt) {
		this.dataProt = dataProt;
	}
	
	public String getDirezione() {
		return direzione;
	}
	public void setDirezione(String direzione) {
		this.direzione = direzione;
	}

	public String getOggetto() {
		return oggetto;
	}
	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj!=null && obj instanceof EcDoquiIterMinProt) {			
			EcDoquiIterMinProt ogg = (EcDoquiIterMinProt)obj;			
			if(this.prot.equals(ogg.getProt()) &&
					this.getDataProt().equals(ogg.getDataProt()) &&
					this.direzione.equals(ogg.getDirezione()) &&
					this.oggetto.equals(ogg.getOggetto()))
			return true;
		}
		return false;
	}

	
}