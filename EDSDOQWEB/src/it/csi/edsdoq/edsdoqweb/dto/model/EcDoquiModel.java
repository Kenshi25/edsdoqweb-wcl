package it.csi.edsdoq.edsdoqweb.dto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import it.csi.edsdoq.edsdoqweb.util.Constants;

@Entity
@Table(name=Constants.EC_DOQUI)
public class EcDoquiModel extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="IDDOQUI")
	private Integer idDoqui;
	
	@Column(name="KEY1")
	private String key1;
	
	@Column(name="KEY2")
	private String key2;
	
	@Column(name="KEY3")
	private String key3;
	
	@Column(name="COD_DOSSIER")
	private String codDossier;
	
	@Column(name="COD_FISC")
	private String codFisc;
	
	@Column(name="TIPO_PROC")
	private String tipoProc;
	
	@Column(name="IN_OUT")
	private String inOut;
	
	@Column(name="TOKEN")
	private String token;
	
	@Column(name="VALIDO")
	private Integer valido;
	
	@Column(name="RAGIONE_SOCIALE")
	private String ragioneSociale;
	
	@Column(name="INDIRIZZO")
	private String indirizzo;
	
	@Column(name="COMUNE")
	private String comune;
	
	@Column(name="UTENTE_APPLICATIVO")
	private String utenteApplicativo;

	public Integer getIdDoqui() {
		return idDoqui;
	}

	public void setIdDoqui(Integer idDoqui) {
		this.idDoqui = idDoqui;
	}

	public String getKey1() {
		return key1;
	}

	public void setKey1(String key1) {
		this.key1 = key1;
	}

	public String getKey2() {
		return key2;
	}

	public void setKey2(String key2) {
		this.key2 = key2;
	}

	public String getKey3() {
		return key3;
	}

	public void setKey3(String key3) {
		this.key3 = key3;
	}

	public String getCodDossier() {
		return codDossier;
	}

	public void setCodDossier(String codDossier) {
		this.codDossier = codDossier;
	}

	public String getCodFisc() {
		return codFisc;
	}

	public void setCodFisc(String codFisc) {
		this.codFisc = codFisc;
	}

	public String getTipoProc() {
		return tipoProc;
	}

	public void setTipoProc(String tipoProc) {
		this.tipoProc = tipoProc;
	}

	public String getInOut() {
		return inOut;
	}

	public void setInOut(String inOut) {
		this.inOut = inOut;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Integer getValido() {
		return valido;
	}

	public void setValido(Integer valido) {
		this.valido = valido;
	}

	public String getRagioneSociale() {
		return ragioneSociale;
	}

	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getComune() {
		return comune;
	}

	public void setComune(String comune) {
		this.comune = comune;
	}

	public String getUtenteApplicativo() {
		return utenteApplicativo;
	}

	public void setUtenteApplicativo(String utenteApplicativo) {
		this.utenteApplicativo = utenteApplicativo;
	}

	@Override
	public String toString() {
		return "EcDoquiModel [idDoqui=" + idDoqui + ", key1=" + key1 + ", key2=" + key2 + ", key3=" + key3
				+ ", codDossier=" + codDossier + ", codFisc=" + codFisc + ", tipoProc=" + tipoProc + ", inOut=" + inOut
				+ ", token=" + token + "]";
	}
}
