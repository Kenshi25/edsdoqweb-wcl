package it.csi.edsdoq.edsdoqweb.dto.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TokenModel {


	private String token;
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	@Override
	public String toString() {
		return "TokenModel [token=" + token + "]";
	}
	
}