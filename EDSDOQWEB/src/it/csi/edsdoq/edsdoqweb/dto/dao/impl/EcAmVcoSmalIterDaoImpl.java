package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoSmalIterDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoSmalIter;

public class EcAmVcoSmalIterDaoImpl extends BaseDaoImpl<EcAmVcoSmalIter, Number> implements EcAmVcoSmalIterDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<EcAmVcoSmalIter> findAllByIdSmalAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcAmVcoSmalIter a " ;
		jpql  +=" where a.idSmal = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
	
		List<EcAmVcoSmalIter> listObj = query.getResultList();
		
		return listObj;
	}

}
