package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oMeteoricheIterModel;

public interface EcH2oMeteoricheIterDao extends BaseDao<EcH2oMeteoricheIterModel, Number>{
	
	List<EcH2oMeteoricheIterModel> findAllByIdMeteoricheAndOggettoNotNull(Integer id);
}
