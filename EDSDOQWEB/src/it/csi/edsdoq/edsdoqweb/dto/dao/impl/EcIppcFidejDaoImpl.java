package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcFidejDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcFidejModel;

public class EcIppcFidejDaoImpl extends BaseDaoImpl<EcIppcFidejModel, Number> implements EcIppcFidejDao{

	@Override
	public List<EcIppcFidejModel> findAllByIdIterProcAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcIppcFidejModel a " ;
		jpql  +=" where a.idIterProc = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcIppcFidejModel> listObj = query.getResultList();
		
		return listObj;
	}

}
