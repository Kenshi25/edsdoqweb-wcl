package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcEventiModel;

public interface EcIppcEventiDao extends BaseDao<EcIppcEventiModel, Number>{

	List<EcIppcEventiModel> findAllByIdIterProcAndOggettoNotNull(Integer id);

}
