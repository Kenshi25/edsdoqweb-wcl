package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcDocModel;

public interface EcDocDao extends BaseDao<EcDocModel, Number> {
	
	public List<EcDocModel> findByKey1AndKey2AndKey3(String key1, String key2, String key3);

}
