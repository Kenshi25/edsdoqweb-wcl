package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiNodiNodoDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiNodiNodoModel;

public class EcDoquiNodiNodoDaoImpl extends BaseDaoImpl<EcDoquiNodiNodoModel, Number> implements EcDoquiNodiNodoDao {

	@Override
	public List<EcDoquiNodiNodoModel> findByIdDoqui(Integer idDoqui) {
		String jpql = " select a from EcDoquiNodiNodoModel a ";
		jpql += " WHERE a.idDoqui = :idDoqui ";
		
		Query q = getEntityManager().createQuery(jpql);
		q.setParameter("idDoqui", idDoqui);
		
		@SuppressWarnings("unchecked")
		List<EcDoquiNodiNodoModel> listaObj = q.getResultList();
		
		return listaObj;
	}

	
}
