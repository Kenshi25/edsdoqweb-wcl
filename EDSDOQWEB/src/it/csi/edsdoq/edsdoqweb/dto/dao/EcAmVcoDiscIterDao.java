package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoDiscIter;

public interface EcAmVcoDiscIterDao extends BaseDao<EcAmVcoDiscIter, Number> {

	List<EcAmVcoDiscIter> findAllByIdDiscAndOggettoNotNull(Integer id);
}
