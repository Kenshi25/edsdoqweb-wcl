package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcOmIterModel;

public interface EcOmIterDao extends BaseDao<EcOmIterModel, Number> {

	List<EcOmIterModel> findAllByIdOliAndOggettoNotNull(Integer id);
	
}
