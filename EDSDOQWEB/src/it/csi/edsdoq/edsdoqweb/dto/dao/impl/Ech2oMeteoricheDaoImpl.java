package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oMeteoricheIterDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oMeteoricheIterModel;

public class Ech2oMeteoricheDaoImpl extends BaseDaoImpl<EcH2oMeteoricheIterModel, Number> implements EcH2oMeteoricheIterDao{

	@Override
	public List<EcH2oMeteoricheIterModel> findAllByIdMeteoricheAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcH2oMeteoricheIterModel a " ;
		jpql  +=" where a.idMeteoriche= :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcH2oMeteoricheIterModel> listObj = query.getResultList();
		
		return listObj;
	}

}
