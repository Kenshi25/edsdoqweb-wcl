package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiNodiAooModel;
import it.csi.edsdoq.edsdoqweb.dto.model.mini.EcDoquiMinModel;

public interface EcDoquiNodiAooDao extends BaseDao<EcDoquiMinModel, Number> {
	
	List<EcDoquiNodiAooModel> findByIdDoqui(Integer idDoqui);
	//List<EcDoquiNodiAooModel> findByIdDoqui(Integer idDoqui, Sort by);

}
