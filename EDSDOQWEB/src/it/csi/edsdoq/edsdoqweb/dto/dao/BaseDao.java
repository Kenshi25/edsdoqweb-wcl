/**
 * 
 */
package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

/**
 * @author Admin
 *
 */
public interface BaseDao<T, ID extends Serializable> {
	
	void setEntityManager(EntityManager entityManager);
	
    T findByKey(Integer key); //ok
    
    T findByKeyObject(ID key); //ok
	
	List<T> findAll(); //ok 
	
	List<T> findAllPaginate(Integer pageNumber, Integer pageSize);//ok
	
	void persist(T entity); //ok
	
	void save(T entity); //ok
	
	void saveNoFlush(T entity); //ok 
	
	Integer getTotalCount() ; //ok
	
	Integer getLastPage( Integer pageSize); //ok
	
	void delete(T entity,Integer id); // ok 
	
	T merge(T entity); //ok 
	
	/**
	 * Agisce su ogni istanza gestita nel persistence context corrente!
	 */
	void flush(); //ok
	
	/**
	 * Agisce su ogni istanza gestita nel persistence context corrente!
	 */
	void clear(); //ok

}
