/**
 * 
 */
package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcAualterDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAuaIterModel;

/**
 * @author lenovo
 *
 */
public class EcAualterDaoImpl extends BaseDaoImpl<EcAuaIterModel, Number> implements EcAualterDao {

	@Override
	public List<EcAuaIterModel> findAllByIdAuaAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcAuaIterModel a " ;
		jpql  +=" where a.idAua = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcAuaIterModel> listObj = query.getResultList();
		
		return listObj;
	}

	

}
