package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiModel;

public class EcDoquiDaoImpl extends BaseDaoImpl<EcDoquiModel, Number> implements EcDoquiDao {

	@Override
	public List<EcDoquiModel> findAllByToken(String token) {
		String jpql = " select a from EcDoquiModel a " ;
		jpql += " where a.token = :token ";
		
		Query q = getEntityManager().createQuery(jpql);
		
		q.setParameter("token", token);
		
		@SuppressWarnings("unchecked")
		List<EcDoquiModel> listObj = q.getResultList();
		
		return listObj;
	}

	

}
