package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoRecsIter;

public interface EcAmVcoRecsIterDao extends BaseDao<EcAmVcoRecsIter, Number>{

	List<EcAmVcoRecsIter> findAllByIdRecsAndOggettoNotNull(Integer id);
}
