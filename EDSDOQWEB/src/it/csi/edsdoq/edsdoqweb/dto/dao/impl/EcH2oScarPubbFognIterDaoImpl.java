package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oScarPubbFognIterDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oScarPubbFognIterModel;

public class EcH2oScarPubbFognIterDaoImpl extends BaseDaoImpl<EcH2oScarPubbFognIterModel, Number> implements EcH2oScarPubbFognIterDao{

	@Override
	public List<EcH2oScarPubbFognIterModel> findAllByIdScaricoAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcH2oScarPubbFognIterModel a " ;
		jpql  +=" where a.idScarico = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcH2oScarPubbFognIterModel> listObj = query.getResultList();
		
		return listObj;
	}

}
