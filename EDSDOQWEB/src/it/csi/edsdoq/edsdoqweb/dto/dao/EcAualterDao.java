package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcAuaIterModel;

public interface EcAualterDao extends BaseDao<EcAuaIterModel, Number> {
	
List<EcAuaIterModel> findAllByIdAuaAndOggettoNotNull(Integer id);
	
}