package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiTempDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiTempModel;

public class EcDoquiTempDaoImpl extends BaseDaoImpl<EcDoquiTempModel, Number> implements EcDoquiTempDao{

	@Override
	public List<EcDoquiTempModel> findByKey(String keyRecord) {
		String jpql  = "select a from EcDoquiTempModel a " ;
		jpql  +=" where a.keyRecord = :KEY_RECORD";
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("KEY_RECORD", keyRecord);
		
		@SuppressWarnings({ "unchecked" })
		List<EcDoquiTempModel> listObj = query.getResultList();
		return listObj;
	}

}
