package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oScarIndustrialiIterDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oScarIndustrialiIterModel;

public class EcH2oScarIndustrialiIterDaoImpl extends BaseDaoImpl<EcH2oScarIndustrialiIterModel, Number> implements EcH2oScarIndustrialiIterDao{

	@Override
	public List<EcH2oScarIndustrialiIterModel> findAllByIdScarAndOggetoNotNull(Integer id) {
		String jpql  = "select a from EcH2oScarIndustrialiIterModel a " ;
		jpql  +=" where a.idScar = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcH2oScarIndustrialiIterModel> listObj = query.getResultList();
		
		return listObj;
	}

}
