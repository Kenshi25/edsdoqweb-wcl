package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oSfioratoriIterDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oSfioratoriIterModel;

public class EcH2oSfioratoriIterDaoImpl extends BaseDaoImpl<EcH2oSfioratoriIterModel, Number> implements EcH2oSfioratoriIterDao{

	@Override
	public List<EcH2oSfioratoriIterModel> findAllByIdSfioratoreAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcH2oSfioratoriIterModel a " ;
		jpql  +=" where a.idSfioratore = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcH2oSfioratoriIterModel> listObj = query.getResultList();
		
		return listObj;
	}

}
