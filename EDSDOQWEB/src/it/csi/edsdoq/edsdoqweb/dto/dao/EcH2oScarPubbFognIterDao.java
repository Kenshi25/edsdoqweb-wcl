package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oScarPubbFognIterModel;

public interface EcH2oScarPubbFognIterDao extends BaseDao<EcH2oScarPubbFognIterModel, Number>{

	List<EcH2oScarPubbFognIterModel> findAllByIdScaricoAndOggettoNotNull(Integer id);
}
