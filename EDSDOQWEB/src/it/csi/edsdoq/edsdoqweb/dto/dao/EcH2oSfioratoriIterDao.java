package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oSfioratoriIterModel;

public interface EcH2oSfioratoriIterDao extends BaseDao<EcH2oSfioratoriIterModel, Number> {

	List<EcH2oSfioratoriIterModel> findAllByIdSfioratoreAndOggettoNotNull(Integer id);
}
