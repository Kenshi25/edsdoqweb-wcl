package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcEventiDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcEventiModel;

public class EcIppcEventiDaoImpl extends BaseDaoImpl<EcIppcEventiModel, Number> implements EcIppcEventiDao{

	@Override
	public List<EcIppcEventiModel> findAllByIdIterProcAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcIppcEventiModel a " ;
		jpql  +=" where a.idIterProc = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcIppcEventiModel> listObj = query.getResultList();
		
		return listObj;
	}

}
