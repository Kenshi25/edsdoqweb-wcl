package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoSmalIter;

public interface EcAmVcoSmalIterDao extends BaseDao<EcAmVcoSmalIter, Number> {
	List<EcAmVcoSmalIter> findAllByIdSmalAndOggettoNotNull(Integer id);
}
