package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiNodiAooDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiNodiAooModel;
import it.csi.edsdoq.edsdoqweb.dto.model.mini.EcDoquiMinModel;

public class EcDoquiNodiAooDaoImpl extends BaseDaoImpl<EcDoquiMinModel, Number> implements EcDoquiNodiAooDao {

	@Override
	public List<EcDoquiNodiAooModel> findByIdDoqui(Integer idDoqui) {
		String jpql = "select a from EcDoquiNodiAooModel a " ;
		jpql +=" where a.idDoqui = :idDoqui ";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("idDoqui", idDoqui);
		
		@SuppressWarnings({ "unchecked" })
		List<EcDoquiNodiAooModel> listaEcDoquiNodiAoo = query.getResultList();
		
		return listaEcDoquiNodiAoo;
	}


	

}
