package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcEalxIterDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcEalxIterModel;

public class EcEalxIterDaoImpl extends BaseDaoImpl<EcEalxIterModel, Number> implements EcEalxIterDao {

	@Override
	public List<EcEalxIterModel> findAllByIdAutorizzAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcEalxIterModel a " ;
		jpql  +=" where a.idAutorizz = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcEalxIterModel> listObj = query.getResultList();
		
		return listObj;
	}

	
}
