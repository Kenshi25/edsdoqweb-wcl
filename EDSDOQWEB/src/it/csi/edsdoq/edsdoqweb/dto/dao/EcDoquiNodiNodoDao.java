package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiNodiNodoModel;

public interface EcDoquiNodiNodoDao extends BaseDao<EcDoquiNodiNodoModel, Number> {
	List <EcDoquiNodiNodoModel> findByIdDoqui(Integer idDoqui);
}
