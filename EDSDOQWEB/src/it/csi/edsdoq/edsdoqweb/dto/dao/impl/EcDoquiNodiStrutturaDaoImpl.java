package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiNodiStrutturaDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiNodiStrutturaModel;

public class EcDoquiNodiStrutturaDaoImpl extends BaseDaoImpl<EcDoquiNodiStrutturaModel,Number> implements EcDoquiNodiStrutturaDao {

	@Override
	public List<EcDoquiNodiStrutturaModel> findByIdDoqui(Integer idDoqui) {
		String jpql = " select a from EcDoquiNodiStrutturaModel a ";
		jpql += " WHERE a.idDoqui = :idDoqui ";
		
		Query q = getEntityManager().createQuery(jpql);
		q.setParameter("idDoqui", idDoqui);
		
		@SuppressWarnings("unchecked")
		List<EcDoquiNodiStrutturaModel> listaObj = q.getResultList();
		
		return listaObj;
	}

	
}
