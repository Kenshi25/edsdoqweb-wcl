package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiNodiStrutturaModel;

public interface EcDoquiNodiStrutturaDao extends BaseDao<EcDoquiNodiStrutturaModel, Number> {
	List<EcDoquiNodiStrutturaModel> findByIdDoqui(Integer idDoqui);
}
