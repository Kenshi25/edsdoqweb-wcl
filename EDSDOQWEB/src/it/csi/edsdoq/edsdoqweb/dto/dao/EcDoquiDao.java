package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiModel;

public interface EcDoquiDao extends BaseDao<EcDoquiModel, Number> {
	List<EcDoquiModel> findAllByToken(String token);
}
