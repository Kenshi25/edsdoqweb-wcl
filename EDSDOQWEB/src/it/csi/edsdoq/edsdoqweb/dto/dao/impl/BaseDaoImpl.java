/**
 * 
 */
package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.FlushModeType;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import it.csi.edsdoq.edsdoqweb.dto.dao.BaseDao;
import it.csi.edsdoq.edsdoqweb.util.Constants;

/**
 * @author Admin
 *
 */
public abstract class BaseDaoImpl<T, ID extends Serializable>  implements BaseDao<T, ID>  {
	
	
	
	protected EntityManager entityManager  = getLocalEntityManager();
	
	protected UserTransaction utx = getUserTransaction();
	
	
	protected Class<T> persistentClass;
	protected Logger logger = Logger.getLogger(Constants.LOGGER_NAME);
	private static final String UNIT_NAME = "edsdoq";
	//protected EntityManager entityManager;

	protected UserTransaction getUserTransaction() {
		UserTransaction txn = null;
		try {
			txn = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
		}
		catch(NamingException e) {
			logger.error(e.getMessage(), e);
		}
		return txn;
	}
	
   
	
	
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public BaseDaoImpl() {
		super();// TODO Auto-generated constructor stub
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	protected EntityManager  getLocalEntityManager() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		em.setFlushMode(FlushModeType.AUTO);
		return em;
	}
	

	public EntityManager getEntityManager() {
		return this.entityManager;
		//return this.entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public Class<T> getPersistentClass() {
		return persistentClass;
	}
	
	public T findByKey(Integer key) {
		return (T) getEntityManager().find(getPersistentClass(), key);
	}
	
	public T findByKeyObject(ID key)  {
		return (T) getEntityManager().find(getPersistentClass(), key);
	}
	
	@Override
	public List<T> findAll() {
		
		String jpql = " select a ";
		jpql += " from " + getPersistentClass().getName() + " a " ;
		
		Query q = getEntityManager().createQuery(jpql);
		
		@SuppressWarnings("unchecked")
		List<T> listaObj = q.getResultList();
		if (listaObj != null && !listaObj.isEmpty()) {
			return listaObj;
		} else {
			return null;
		}
	}
	
	@Override
	public List<T> findAllPaginate(Integer pageNumber, Integer pageSize) {
		
		String sql = " select a ";
		sql += " from " + getPersistentClass().getName() + " a ";
		
		Query q = getEntityManager().createQuery(sql);
		q.setFirstResult((pageNumber-1) * pageSize);
		q.setMaxResults(pageSize);
	
		@SuppressWarnings("unchecked")
		List<T> listaObj = q.getResultList();
		if (listaObj != null && !listaObj.isEmpty()) {
			return listaObj;
		} else {
			return null;
		}
	}

	@Override
	public Integer getTotalCount() {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> query =  cb.createQuery(Long.class);
		query.select(cb.count(query.from(getPersistentClass())));
		Long result = getEntityManager().createQuery(query).getSingleResult();
		Integer count = new Integer(result.intValue());
		return count;
	}

	public Integer getLastPage( Integer pageSize) {
		return (int) ((getTotalCount() / pageSize) + 1);
	}

	
	public void flush() {
		getEntityManager().flush();
	}
	
	public void clear() {
		getEntityManager().clear();
	}
	
	
	
	public void persist(T entity) {
		
		logger.debug("perstist Entity start");
		try {
			try {
				utx.begin();
			} catch (NotSupportedException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			getEntityManager().joinTransaction();
			getEntityManager().persist(entity);
			//getEntityManager().flush();
			try {
				utx.commit();
				getEntityManager().close();
			} catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
		} catch (RuntimeException e) {
			logger.error("save failed", e);
			try {
				utx.rollback();
			} catch (IllegalStateException | SecurityException | SystemException e1) {
				logger.error(e1.getMessage(),e1);
			}
			throw e;
		}
		
	}
	
	public void save(T entity)  {
		logger.debug("save dirty instance");
		try {
			try {
				utx.begin();
			} catch (NotSupportedException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			getEntityManager().joinTransaction();
			getEntityManager().persist(entity);
			try {
				utx.commit();
				getEntityManager().close();
			} catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			
		} catch (RuntimeException re) {
			try {
				utx.rollback();
			} catch (IllegalStateException | SecurityException | SystemException e1) {
				logger.error(e1.getMessage(),e1);
			}
			throw re;
		}
	}

	
	public void saveNoFlush(T entity) {
		logger.debug("save dirty instance");
		try {
			try {
				utx.begin();
			} catch (NotSupportedException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			getEntityManager().joinTransaction();
			getEntityManager().persist(entity);
			try {
				utx.commit();
				getEntityManager().close();
			} catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			logger.debug("save successful");
		} catch (RuntimeException re) {
			logger.error("save failed", re);
			//getEntityManager().getTransaction().rollback();
			try {
				utx.rollback();
			} catch (IllegalStateException | SecurityException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			throw re;
		}
	}
	
	
	public void delete(T entity) {
		logger.debug("deleting instance");
		try {
			try {
				utx.begin();
			} catch (NotSupportedException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			getEntityManager().joinTransaction();
			getEntityManager().remove(entity);
			try {
				utx.commit();
				getEntityManager().close();
			} catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			logger.debug("delete successful");
		} catch (RuntimeException re) {
			logger.error("delete failed", re);
			//getEntityManager().getTransaction().rollback();
			try {
				utx.rollback();
			} catch (IllegalStateException | SecurityException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			throw re;
		}
	}
	
	public T merge(T entity) {
		logger.debug("merging instance");
		try {
			try {
				utx.begin();
				
			} catch (NotSupportedException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			getEntityManager().joinTransaction();
			T result = (T) getEntityManager().merge(entity);
			try {
				utx.commit();
				getEntityManager().close();
			} catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			
			logger.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			logger.error("merge failed", re);
			//getEntityManager().getTransaction().rollback();
			try {
				utx.rollback();
			} catch (IllegalStateException | SecurityException | SystemException e) {
				logger.error(e.getMessage(),e);
			}
			throw re;
		}
	}

}
