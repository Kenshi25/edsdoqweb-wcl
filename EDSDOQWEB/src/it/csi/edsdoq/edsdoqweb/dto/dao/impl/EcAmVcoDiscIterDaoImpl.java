package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoDiscIterDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoDiscIter;

public class EcAmVcoDiscIterDaoImpl extends BaseDaoImpl<EcAmVcoDiscIter, Number> implements EcAmVcoDiscIterDao {

	@Override
	public List<EcAmVcoDiscIter> findAllByIdDiscAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcAmVcoDiscIter a " ;
		jpql  +=" where a.idDisc = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcAmVcoDiscIter> listObj = query.getResultList();
		
		return listObj;
	}

}
