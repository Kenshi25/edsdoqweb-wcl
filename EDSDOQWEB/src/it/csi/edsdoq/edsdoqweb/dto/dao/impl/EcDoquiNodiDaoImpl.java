package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiNodiDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiNodiModel;

public class EcDoquiNodiDaoImpl extends BaseDaoImpl<EcDoquiNodiModel, Number> implements EcDoquiNodiDao {

	@Override
	public List<EcDoquiNodiModel> findByIdDoqui(Integer idDoqui) {
		String jpql = " select a from EcDoquiNodiModel a " ;
		jpql += " where a.idDoqui = :idDoqui ";
		
		Query q = getEntityManager().createQuery(jpql);
		
		q.setParameter("idDoqui", idDoqui);
		
		@SuppressWarnings("unchecked")
		List<EcDoquiNodiModel> listObj = q.getResultList();
		
		return listObj;
	}



}
