package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcOmIterDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcOmIterModel;

public class EcOmIterDaoImpl extends BaseDaoImpl<EcOmIterModel, Number> implements EcOmIterDao {

	@Override
	public List<EcOmIterModel> findAllByIdOliAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcOmIterModel a " ;
		jpql  +=" where a.idOli = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcOmIterModel> listObj = query.getResultList();
		
		return listObj;
	}

}
