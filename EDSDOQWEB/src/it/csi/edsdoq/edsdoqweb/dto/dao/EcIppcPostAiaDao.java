package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcPostAiaModel;

public interface EcIppcPostAiaDao extends BaseDao<EcIppcPostAiaModel, Number> {
	List<EcIppcPostAiaModel> findAllByIdIterProcAndOggettoNotNull(Integer id);

}
