package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcPostAiaDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcPostAiaModel;

public class EcIppcPostAiaDaoImpl extends BaseDaoImpl<EcIppcPostAiaModel, Number> implements EcIppcPostAiaDao{

	@Override
	public List<EcIppcPostAiaModel> findAllByIdIterProcAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcIppcPostAiaModel a " ;
		jpql  +=" where a.idIterProc = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcIppcPostAiaModel> listObj = query.getResultList();
		
		return listObj;
	}

}
