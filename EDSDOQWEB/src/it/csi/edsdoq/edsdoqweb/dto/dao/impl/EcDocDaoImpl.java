/**
 * 
 */
package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcDocDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDocModel;

/**
 * @author lenovo
 *
 */
public class EcDocDaoImpl extends BaseDaoImpl<EcDocModel, Number> implements EcDocDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<EcDocModel> findByKey1AndKey2AndKey3(String key1, String key2, String key3) {
		String sql = "Select a from EcDocModel a ";
		sql += "where a.key1 = :Key1 ";
		sql += "and a.key2 = :Key2 ";
		sql += "and a.key3 = :Key3 ";

		Query query = getEntityManager().createQuery(sql);
		query.setParameter("Key1", key1);
		query.setParameter("Key2", key2);
		query.setParameter("Key3", key3);

		List<EcDocModel> listaEcDoc = new ArrayList<EcDocModel>();
		try {
			listaEcDoc = query.getResultList();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return listaEcDoc;
	}

}
