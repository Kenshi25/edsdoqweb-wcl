package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcFidejModel;

public interface EcIppcFidejDao extends BaseDao<EcIppcFidejModel, Number> {

	List<EcIppcFidejModel> findAllByIdIterProcAndOggettoNotNull(Integer id);
	
}
