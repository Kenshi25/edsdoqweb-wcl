package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oScarIndustrialiIterModel;

public interface EcH2oScarIndustrialiIterDao extends BaseDao<EcH2oScarIndustrialiIterModel, Number> {

	List<EcH2oScarIndustrialiIterModel> findAllByIdScarAndOggetoNotNull(Integer id);
}
