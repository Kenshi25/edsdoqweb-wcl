package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcEalxIterModel;

public interface EcEalxIterDao extends BaseDao<EcEalxIterModel, Number> {

	List<EcEalxIterModel> findAllByIdAutorizzAndOggettoNotNull(Integer id);

}
