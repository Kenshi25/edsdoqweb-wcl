package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoRecsIterDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoRecsIter;

public class EcAmVcoRecsIterDaoImpl extends BaseDaoImpl<EcAmVcoRecsIter, Number> implements EcAmVcoRecsIterDao{

	@Override
	public List<EcAmVcoRecsIter> findAllByIdRecsAndOggettoNotNull(Integer id) {
		String jpql  = "select a from EcAmVcoRecsIter a " ;
		jpql  +=" where a.idRecs = :id";
		jpql +=" and a.oggetto is not null";
		
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);
		
		@SuppressWarnings({ "unchecked" })
		List<EcAmVcoRecsIter> listObj = query.getResultList();
		
		return listObj;
	}

}
