package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiTempModel;

public interface EcDoquiTempDao extends BaseDao<EcDoquiTempModel, Number> {
	List<EcDoquiTempModel> findByKey(String key);
}
