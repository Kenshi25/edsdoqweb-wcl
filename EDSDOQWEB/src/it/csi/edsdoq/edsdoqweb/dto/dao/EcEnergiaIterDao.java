package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcEnergiaIterModel;

public interface EcEnergiaIterDao extends BaseDao<EcEnergiaIterModel, Number>{
	
	List<EcEnergiaIterModel> findAllByIdEnergiaAndOggettoNotNull(Integer id);

}
