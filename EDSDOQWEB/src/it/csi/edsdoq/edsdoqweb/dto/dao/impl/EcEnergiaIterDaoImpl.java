package it.csi.edsdoq.edsdoqweb.dto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcEnergiaIterDao;
import it.csi.edsdoq.edsdoqweb.dto.model.EcEnergiaIterModel;

public class EcEnergiaIterDaoImpl extends BaseDaoImpl<EcEnergiaIterModel, Number> implements EcEnergiaIterDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<EcEnergiaIterModel> findAllByIdEnergiaAndOggettoNotNull(Integer id) {
		String jpql = "select a from EcEnergiaIterModel a ";
		jpql += " where a.idEnergia = :id";
		jpql += " and a.oggetto is not null";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("id", id);

		List<EcEnergiaIterModel> listObj = query.getResultList();

		return listObj;
	}

}
