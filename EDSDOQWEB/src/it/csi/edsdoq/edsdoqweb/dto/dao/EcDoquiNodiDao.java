package it.csi.edsdoq.edsdoqweb.dto.dao;

import java.util.List;

import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiNodiModel;

public interface EcDoquiNodiDao extends BaseDao<EcDoquiNodiModel, Number> {
	List<EcDoquiNodiModel> findByIdDoqui(Integer idDoqui);	
}
