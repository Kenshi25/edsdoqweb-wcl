package it.csi.edsdoq.edsdoqweb.business;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import it.csi.edsdoq.edsdoqweb.business.endpoint.EdsdoqWebEndPoint;

@ApplicationPath("/")
public class EdsdoqWebApplication extends Application {
	private Set<Object> singletons = new HashSet<Object>();
//	private Set<Class<?>> empty = new HashSet<Class<?>>();

	public EdsdoqWebApplication() {
		this.singletons.add(new EdsdoqWebEndPoint());
//		singletons.add(new SpringInjectorInterceptor());
//
//		for (Object c : singletons) {
//			if (c instanceof SpringSupportedResource) {
//				SpringApplicationContextHelper.registerRestEasyController(c);
//			}
//		}
	}

//	@Override
//	public Set<Class<?>> getClasses() {
//		return empty;
//	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
