package it.csi.edsdoq.edsdoqweb.business.endpoint;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import org.springframework.stereotype.Component;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoDiscIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoRecsIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcAmVcoSmalIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcAualterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiNodiAooDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiNodiNodoDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiNodiStrutturaDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiTempDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcEalxIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcEnergiaIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oMeteoricheIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oScarIndustrialiIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oScarPubbFognIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcH2oSfioratoriIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcEventiDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcFidejDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcIppcPostAiaDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.EcOmIterDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAmVcoDiscIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAmVcoRecsIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAmVcoSmalIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcAualterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcDoquiDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcDoquiNodiAooDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcDoquiNodiNodoDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcDoquiNodiStrutturaDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcDoquiTempDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcEalxIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcEnergiaIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcH2oScarIndustrialiIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcH2oScarPubbFognIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcH2oSfioratoriIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcIppcEventiDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcIppcFidejDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcIppcPostAiaDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcOmIterDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.Ech2oMeteoricheDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoDiscIter;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoRecsIter;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAmVcoSmalIter;
import it.csi.edsdoq.edsdoqweb.dto.model.EcAuaIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiNodiAooModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiNodiNodoModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiNodiStrutturaModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiTempModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcEalxIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcEnergiaIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oMeteoricheIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oScarIndustrialiIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oScarPubbFognIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcH2oSfioratoriIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcEventiModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcFidejModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcIppcPostAiaModel;
import it.csi.edsdoq.edsdoqweb.dto.model.EcOmIterModel;
import it.csi.edsdoq.edsdoqweb.dto.model.RispostaModel;
import it.csi.edsdoq.edsdoqweb.dto.model.TokenModel;
import it.csi.edsdoq.edsdoqweb.dto.model.mini.EcDoquiIterMinProt;
import it.csi.edsdoq.edsdoqweb.dto.model.mini.EcDoquiMinModel;
import it.csi.edsdoq.edsdoqweb.dto.model.mini.EcDoquiNodiAooMinModel;
import it.csi.edsdoq.edsdoqweb.dto.model.mini.EcDoquiNodiNodoMinModel;
import it.csi.edsdoq.edsdoqweb.dto.model.mini.EcDoquiNodiStrutturaMinModel;
import it.csi.edsdoq.edsdoqweb.util.ActaClientSrvUtils;
import it.csi.edsdoq.edsdoqweb.util.BackendService;
import it.csi.edsdoq.edsdoqweb.util.Constants;
import it.csi.edsdoq.edsdoqweb.util.ServiziAcaris;
import it.csi.edsdoq.edsdoqweb.util.TestUtils;
import it.csi.edsdoq.edsdoqweb.utils.sort.GenericComparator;
import it.csi.edsdoq.edsdoqweb.utils.sort.SortType;
import it.csi.taif.taifweb.dto.common.UserInfo;
import it.doqui.acta.acaris.objectservice.AcarisException;
import it.doqui.acta.actasrv.dto.acaris.type.archive.EnumRelationshipDirectionType;
import it.doqui.acta.actasrv.dto.acaris.type.archive.EnumRelationshipObjectType;
import it.doqui.acta.actasrv.dto.acaris.type.archive.RelationshipPropertiesType;
import it.doqui.acta.actasrv.dto.acaris.type.common.AcarisContentStreamType;
import it.doqui.acta.actasrv.dto.acaris.type.common.EnumStreamId;
import it.doqui.acta.actasrv.dto.acaris.type.common.ObjectIdType;
import it.doqui.acta.actasrv.dto.acaris.type.common.PrincipalIdType;
import it.doqui.acta.actasrv.dto.acaris.type.common.PropertyFilterType;

@Path("/")
@Component
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class EdsdoqWebEndPoint {

	Logger logger = Logger.getLogger(Constants.LOGGER_NAME);

	@GET
	@Path("/getdoquitemp/{ID_DOQUI_TEMP}")
	public Response getDoquiTemp(@PathParam(value = "ID_DOQUI_TEMP") String ID_DOQUI_TEMP) {
		RispostaModel risp = new RispostaModel();

		// Istanzio il Dao e il model
		EcDoquiTempDao ecdoqui = new EcDoquiTempDaoImpl();
		EcDoquiTempModel ecdTemp = ecdoqui.findByKey(ID_DOQUI_TEMP).get(0);

		// Salvo lo status dell'oggetto
		int ecdStatus = ecdTemp.getStatus();
		if (ecdStatus == 1) {
			ecdoqui.delete(ecdTemp);

			if (ecdStatus == 1) {
				/****************************************/
				/** update validita' token su EC_DOQUI **/
				/****************************************/
				String token = TestUtils.getToken(ID_DOQUI_TEMP);
				ActaClientSrvUtils acta = new ActaClientSrvUtils();
				acta.updateToken(token);
			}
		}

		risp.setCodice(ecdStatus);
		risp.setMessaggio(ecdTemp.getMessaggio());

		return Response.status(200).entity(risp).build();
	}

	@GET
	@Path("/acaris/{ID_AOO}/{ID_NODO}/{ID_STRUTTURA}/{INOUT}/{PROTOCOLLO}/{TOKEN}")
	public Response serviziAcaris(@Context ServletContext srvCtx, @PathParam(value = "ID_AOO") long ID_AOO,
			@PathParam(value = "ID_NODO") long ID_NODO, @PathParam(value = "ID_STRUTTURA") long ID_STRUTTURA,
			@PathParam(value = "INOUT") String INOUT, @PathParam(value = "PROTOCOLLO") String PROTOCOLLO,
			@PathParam(value = "TOKEN") String TOKEN) {

		RispostaModel risposta = new RispostaModel();

		String actaServer = srvCtx.getInitParameter("acta.server");
		int actaPort = Integer.valueOf(srvCtx.getInitParameter("acta.port"));
		String actaContext = srvCtx.getInitParameter("acta.context");

		ServiziAcaris sa = new ServiziAcaris(actaServer, actaContext, actaPort, Constants.MTOM_ENABLED);
		ActaClientSrvUtils actaClientSrvUtils = new ActaClientSrvUtils(sa);

		String repoName = srvCtx.getInitParameter("repo.name");
		/** getRepositoryId */
		ObjectIdType repoId = sa.getRepositoryId(repoName);

		/** getPrincipalId */
		PrincipalIdType prinId;
		try {
			prinId = sa.getPrincipalExt(repoId, Constants.COD_FISCALE, ID_AOO, ID_STRUTTURA, ID_NODO,
					Constants.APP_KEY);
		} catch (it.doqui.acta.acaris.backofficeservice.AcarisException acEx) {
			logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
			risposta.setCodice(1);
			risposta.setMessaggio("Impossibile recuperare il principal Id");
			return Response.status(200).entity(risposta).build();
		}

		/******************************************/
		/** Gestione del Dossier e del Fascicolo **/
		/******************************************/
		// getID della serieDossier = AZICONPOS
		ObjectIdType serieDossierId = new ObjectIdType();
		try {
			serieDossierId = actaClientSrvUtils.getSerieDossierId(repoId, prinId);
		} catch (it.doqui.acta.acaris.objectservice.AcarisException acEx) {
			logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
			risposta.setCodice(1);
			risposta.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
			return Response.status(200).entity(risposta).build();
		}

		// controllo esistenza Dossier all'interno della serieDossier
		ObjectIdType dossierId = null;
		ObjectIdType fascicoloId = null;

		try {
			dossierId = actaClientSrvUtils.getDossierId(TOKEN, repoId, prinId, serieDossierId);
		} catch (AcarisException acEx) {
			logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
		}

		if (null != dossierId) {
			logger.info("********** Dossier presente - Ricerca Fascicolo");

			// controllo esistenza Fasciolo
			try {
				fascicoloId = actaClientSrvUtils.getFascicoloId(TOKEN, repoId, prinId, dossierId, ID_AOO, ID_STRUTTURA,
						ID_NODO);
			} catch (AcarisException acEx) {
				logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
						+ acEx.getFaultInfo().getTechnicalInfo());
			}

			if (null == fascicoloId) {
				logger.info("********** Fascicolo non presente - Creazione Fascicolo");
				try {
					fascicoloId = actaClientSrvUtils.createFascicolo(repoId, prinId, dossierId, TOKEN, ID_AOO,
							ID_STRUTTURA, ID_NODO);
				} catch (AcarisException acEx) {
					logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
							+ acEx.getFaultInfo().getTechnicalInfo());
					risposta.setCodice(1);
					risposta.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
							+ acEx.getFaultInfo().getTechnicalInfo());
					return Response.status(200).entity(risposta).build();
				} catch (it.doqui.acta.acaris.managementservice.AcarisException acEx) {
					logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
							+ acEx.getFaultInfo().getTechnicalInfo());
					risposta.setCodice(1);
					risposta.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
							+ acEx.getFaultInfo().getTechnicalInfo());
					return Response.status(200).entity(risposta).build();
				}
				logger.info("********** Fascicolo creato");
			} else {
				logger.info("********** Fascicolo trovato");
			}

		} else {
			logger.info("********** Dossier non presente - Creazione Dossier");

			try {
				dossierId = actaClientSrvUtils.createDossier(repoId, prinId, serieDossierId, TOKEN, ID_AOO,
						ID_STRUTTURA, ID_NODO);
			} catch (it.doqui.acta.acaris.objectservice.AcarisException acEx) {
				logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
						+ acEx.getFaultInfo().getTechnicalInfo());
				risposta.setCodice(1);
				risposta.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
						+ acEx.getFaultInfo().getTechnicalInfo());
				return Response.status(200).entity(risposta).build();
			} catch (it.doqui.acta.acaris.managementservice.AcarisException acEx) {
				logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
						+ acEx.getFaultInfo().getTechnicalInfo());
				risposta.setCodice(1);
				risposta.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
						+ acEx.getFaultInfo().getTechnicalInfo());
				return Response.status(200).entity(risposta).build();
			}
			logger.info("********** Dossier Creato");
			logger.info("********** Creazione Fascicolo");
			try {
				fascicoloId = actaClientSrvUtils.createFascicolo(repoId, prinId, dossierId, TOKEN, ID_AOO, ID_STRUTTURA,
						ID_NODO);
			} catch (AcarisException acEx) {
				logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
						+ acEx.getFaultInfo().getTechnicalInfo());
				risposta.setCodice(1);
				risposta.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
						+ acEx.getFaultInfo().getTechnicalInfo());
				return Response.status(200).entity(risposta).build();
			} catch (it.doqui.acta.acaris.managementservice.AcarisException acEx) {
				logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
						+ acEx.getFaultInfo().getTechnicalInfo(), acEx);
				risposta.setCodice(1);
				risposta.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
						+ acEx.getFaultInfo().getTechnicalInfo());
				return Response.status(200).entity(risposta).build();
			}
			logger.info("********** Fascicolo Creato");
		}

		/*******************************************/
		/** Preparazione per il salvataggio su DB **/
		/*******************************************/
		List<String> listaProtocollo = Arrays.asList(PROTOCOLLO.split("-"));

		// numero registrazione e' su 8 cifre
		String codiceProtocollo = StringUtils.leftPad(listaProtocollo.get(0), 8, '0');
		String annoProtocollo = listaProtocollo.get(1);

		/**************************/
		/** getIdClassificazione **/
		/**************************/
		ObjectIdType classificazioneId = new ObjectIdType();

		try {
			classificazioneId = actaClientSrvUtils.ricercaClassificazioneProtocollo(repoId, prinId, ID_AOO,
					codiceProtocollo, annoProtocollo);
		} catch (it.doqui.acta.acaris.officialbookservice.AcarisException acEx) {
			logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
			risposta.setCodice(1);
			risposta.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
			return Response.status(200).entity(risposta).build();
		}

		/******************/
		/** moveDocument **/
		/******************/
		// per spostare una classificazione dal fascicolo temporaneo all'aggregazione di
		// interesse
		boolean flag = false;
		try {
			flag = actaClientSrvUtils.fascicolaDocumento(repoId, prinId, classificazioneId, fascicoloId);
		} catch (it.doqui.acta.acaris.navigationservice.AcarisException acEx) {
			logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
			risposta.setCodice(1);
			risposta.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
			return Response.status(200).entity(risposta).build();
		} catch (AcarisException acEx) {
			risposta.setCodice(1);
			risposta.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
			logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
			return Response.status(200).entity(risposta).build();
		}

		if (!flag) {
			risposta.setCodice(1);
			risposta.setMessaggio("Errore durante la fascicolazione del documento");
			return Response.status(200).entity(risposta).build();
		}

		/******************************************/
		/** Preparazione per il multithread **/
		/******************************************/
		EcDoquiTempDaoImpl ecdoqui = new EcDoquiTempDaoImpl();
		String kr = TestUtils.getCode(TOKEN);
		// salvo il valore
		EcDoquiTempModel entity = new EcDoquiTempModel();
		entity.setStatus(0);
		entity.setKeyRecord(kr);
		ecdoqui.persist(entity);

		EcDoquiTempDaoImpl ecdoqui2 = new EcDoquiTempDaoImpl();
		EcDoquiTempModel ecdTemp = (EcDoquiTempModel) ecdoqui2.findByKey(kr).get(0);

		HashMap<String, Object> mappa = TestUtils.getDaoMappa(TOKEN);

		/***********************************/
		/** getIdChildren - docIdVirtuale **/
		/***********************************/
		List<ObjectIdType> listaDocId = new ArrayList<ObjectIdType>();
		try {
			listaDocId = actaClientSrvUtils.getListaDocumentoObjectIdFromClassificazioneObjectId(repoId, prinId,
					classificazioneId);
		} catch (it.doqui.acta.acaris.navigationservice.AcarisException acEx) {
			logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
			ecdTemp.setStatus(2);
			ecdTemp.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
					+ acEx.getFaultInfo().getTechnicalInfo());
		}

		HashMap<Integer, Object> mappaDoc = TestUtils.getDaoDocMappa(listaDocId.size());
		final List<ObjectIdType> listaDocIdFinal = listaDocId;
		// Faccio partire il thread parallelo
		new Thread(new Runnable() {
			public void run() {
				/******************************************/
				/** getObjectRelationships - docIdFisico **/
				/******************************************/
				List<ObjectIdType> listaDocIdFisico = new ArrayList<ObjectIdType>();
				if (null != listaDocIdFinal || !listaDocIdFinal.isEmpty()) {
					// Per prendere il documento fisico da passare al Content stream
					EnumRelationshipObjectType typeId = EnumRelationshipObjectType.DOCUMENT_COMPOSITION_PROPERTIES_TYPE;
					EnumRelationshipDirectionType direction = EnumRelationshipDirectionType.SOURCE;
					PropertyFilterType filter2 = TestUtils.getPropertyFilterAll();
					RelationshipPropertiesType[] relathionsipPropertyes = null;

					for (ObjectIdType docId : listaDocIdFinal) {
						try {
							relathionsipPropertyes = sa.getRelationshipsServicePort().getObjectRelationships(repoId,
									prinId, docId, typeId, direction, filter2);
							for (RelationshipPropertiesType rpt : relathionsipPropertyes) {
								listaDocIdFisico.add(rpt.getTargetId());
							}
						} catch (it.doqui.acta.acaris.relationshipsservice.AcarisException acEx) {
							logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
									+ acEx.getFaultInfo().getTechnicalInfo());
							ecdTemp.setStatus(2);
							ecdTemp.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
									+ acEx.getFaultInfo().getTechnicalInfo());
						}catch (Exception e) {
							logger.error(e.getMessage());
							ecdTemp.setStatus(2);
							ecdTemp.setMessaggio(e.getMessage());
						}
					}
				}

				/**********************/
				/** getContentStream **/
				/**********************/
				boolean flagCoSt = true;
				if (null != listaDocIdFisico || !listaDocIdFisico.isEmpty()) {
					EnumStreamId streamId = EnumStreamId.PRIMARY;
					AcarisContentStreamType[] contentStream = {};
					int numDoc = 1;
					for (ObjectIdType docIdFisico : listaDocIdFisico) {
						try {
							contentStream = sa.getObjectServicePort().getContentStream(repoId, docIdFisico, prinId,
									streamId);

							for (AcarisContentStreamType cs : contentStream) {

								logger.info("************ FileName: " + cs.getFilename() + " |FileType: "
										+ cs.getMimeType().value());

								/*****************/
								/** save EC_DOC **/
								/*****************/

								actaClientSrvUtils.saveFileV2(TOKEN, cs, mappaDoc, numDoc);
							}

						} catch (AcarisException acEx) {
							logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
									+ acEx.getFaultInfo().getTechnicalInfo());
							ecdTemp.setStatus(2);
							ecdTemp.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
									+ acEx.getFaultInfo().getTechnicalInfo());
							flagCoSt = false;
						}catch (Exception e) {
							logger.error(e.getMessage());
							ecdTemp.setStatus(2);
							ecdTemp.setMessaggio(e.getMessage());
						}
						numDoc++;
					}
				}

				/***************************************/
				/** update tabella=Key2 idRecord=Key3 **/
				/***************************************/
				// getOggetto
				ObjectIdType oggId = new ObjectIdType();
				String oggetto = null;
				if (flagCoSt) {
					try {
						oggId = actaClientSrvUtils.getOggetto(repoId, prinId, ID_AOO, codiceProtocollo, annoProtocollo);
						oggetto = oggId.getValue();
						System.out.println(oggetto);

						try {
							actaClientSrvUtils.updateTabellaV2(TOKEN, repoId, prinId, listaProtocollo, oggetto, ID_AOO,
									mappa);
						} catch (Exception e) {
							logger.error(e.getMessage());
							ecdTemp.setStatus(2);
							ecdTemp.setMessaggio(e.getMessage());
						}

					} catch (it.doqui.acta.acaris.officialbookservice.AcarisException acEx) {
						logger.error("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
								+ acEx.getFaultInfo().getTechnicalInfo());
						ecdTemp.setStatus(2);
						ecdTemp.setMessaggio("ErrorCode: " + acEx.getFaultInfo().getErrorCode() + " | "
								+ acEx.getFaultInfo().getTechnicalInfo());
					}catch (Exception e) {
						logger.error(e.getMessage());
						ecdTemp.setStatus(2);
						ecdTemp.setMessaggio(e.getMessage());
					}
				}
				// se lo status � 2 (errore) faccio merge diretto
				if (ecdTemp.getStatus() == 2) {
					ecdoqui2.merge(ecdTemp);
				} else {
					// altrimenti setto a 1 e faccio merge
					ecdTemp.setStatus(1);
					ecdoqui2.merge(ecdTemp);
				}
			}
		}).start();

		// risposta positiva se va tutto bene
		risposta.setCodice(0);
		risposta.setMessaggio(kr);
		return Response.status(200).entity(risposta).build();
	}

	@GET
	@Path("/postoggetto/{ID_AOO}/{ID_NODO}/{ID_STRUTTURA}/{INOUT}/{PROTOCOLLO}/{TOKEN}")
	public Response getOggetto(@Context ServletContext srvCtx, @PathParam(value = "ID_AOO") long ID_AOO,
			@PathParam(value = "ID_NODO") long ID_NODO, @PathParam(value = "ID_STRUTTURA") long ID_STRUTTURA,
			@PathParam(value = "INOUT") String INOUT, @PathParam(value = "PROTOCOLLO") String PROTOCOLLO,
			@PathParam(value = "TOKEN") String TOKEN) {

		RispostaModel risposta = new RispostaModel();

		String actaServer = srvCtx.getInitParameter("acta.server");
		int actaPort = Integer.valueOf(srvCtx.getInitParameter("acta.port"));
		String actaContext = srvCtx.getInitParameter("acta.context");

		ServiziAcaris sa = new ServiziAcaris(actaServer, actaContext, actaPort, Constants.MTOM_ENABLED);
		ActaClientSrvUtils actaClientSrvUtils = new ActaClientSrvUtils(sa);

		String repoName = srvCtx.getInitParameter("repo.name");
		/** getRepositoryId */
		ObjectIdType repoId = sa.getRepositoryId(repoName);

		/** Principal Id */
		PrincipalIdType prinId = new PrincipalIdType();
		try {
			prinId = sa.getPrincipalExtForOggetto(repoId, Constants.COD_FISCALE, ID_AOO, Constants.APP_KEY);
		} catch (it.doqui.acta.acaris.backofficeservice.AcarisException e1) {
			logger.error(e1.getMessage(), e1);
		}

		// gestire principal in error (null)
		if (prinId == null) {
			risposta.setCodice(1);
			risposta.setMessaggio("Impossibile recuperare il principal Id");
			return Response.status(200).entity(risposta).build();
		}

		try {
			// *************************************
			System.out.println("[RicercaProtocolloEClassificazione::main]: ********************** inizio");

			String[] protocolloArray = PROTOCOLLO.split("-");
			String anno = protocolloArray[1];
			// numero registrazione su 8 cifre
			String codiceRegistrazioneProtocollo = StringUtils.leftPad(protocolloArray[0], 8, '0');

			ObjectIdType classObjectId = actaClientSrvUtils.getOggetto(repoId, prinId, ID_AOO,
					codiceRegistrazioneProtocollo, anno);
			// ************************************
			if ((classObjectId == null)) {
				risposta.setCodice(1);
				risposta.setMessaggio("Numero e/o Data di Protocollo specificati non sono validi");
				return Response.status(200).entity(risposta).build();
			} else {
				risposta.setCodice(0);
				risposta.setMessaggio(classObjectId.getValue());
				return Response.status(200).entity(risposta).build();
			}
		} catch (it.doqui.acta.acaris.officialbookservice.AcarisException e) {
			logger.debug(e.getFaultInfo());
			risposta.setCodice(1);
			risposta.setMessaggio("Numero e/o Data di Protocollo specificati non sono validi");
			return Response.status(200).entity(risposta).build();
		}
	}

	/*****************************/
	/***** Servizi EDSDOQWEB *****/
	/*****************************/

	@GET
	@Path("/echo")
	public Response getProva() {
		return Response.status(200).entity("echo ok").build();
	}

	@POST
	@Path("/postdirezione")
	public Response getDirezione(TokenModel token) {

		RispostaModel risposta = new RispostaModel();
		try {
			EcDoquiModel ecDoqui = new EcDoquiModel();
			EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();
			ecDoqui = ecDoquiDao.findAllByToken(token.getToken()).get(0);
			return Response.status(200).entity("{\"direzione\":\"" + ecDoqui.getInOut() + "\"}").build();
		} catch (Exception e) {
			risposta.setCodice(1);
			risposta.setMessaggio(e.getMessage());
			return Response.status(404).entity(risposta).build();
		}
	}

	@POST
	@Path("/postaoo")
	public Response getEcDoquiNodiAoo(TokenModel token) {

		RispostaModel risposta = new RispostaModel();
		EcDoquiModel ecDoqui = new EcDoquiModel();
		EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();
		EcDoquiNodiAooDao ecDoquiNodiAooDao = new EcDoquiNodiAooDaoImpl();
		try {
			ecDoqui = ecDoquiDao.findAllByToken(token.getToken()).get(0);

			List<EcDoquiNodiAooModel> lista = ecDoquiNodiAooDao.findByIdDoqui(ecDoqui.getIdDoqui());
			GenericComparator.sortList(lista, "opzione", SortType.ASC);
			List<EcDoquiNodiAooMinModel> listaMin = new ArrayList<EcDoquiNodiAooMinModel>();
			for (EcDoquiNodiAooModel ec : lista) {
				EcDoquiNodiAooMinModel aooMin = new EcDoquiNodiAooMinModel(ec.getDescrAoo(), ec.getCodAoo());
				if (!listaMin.contains(aooMin))
					listaMin.add(aooMin);
			}
			return Response.status(200).entity(listaMin).build();

		} catch (Exception e) {
			risposta.setCodice(1);
			risposta.setMessaggio(e.getMessage());
			return Response.status(404).entity(risposta).build();
		}
	}

	@POST
	@Path("/postnodo")
	public Response getEcDoquiNodiNodo(TokenModel token) {

		RispostaModel risposta = new RispostaModel();
		EcDoquiModel ecDoqui = new EcDoquiModel();
		EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();
		EcDoquiNodiNodoDao ecDoquiNodiNodoDao = new EcDoquiNodiNodoDaoImpl();
		try {
			ecDoqui = ecDoquiDao.findAllByToken(token.getToken()).get(0);
			List<EcDoquiNodiNodoModel> lista = ecDoquiNodiNodoDao.findByIdDoqui(ecDoqui.getIdDoqui());
			GenericComparator.sortList(lista, "opzione", SortType.ASC);

			List<EcDoquiNodiNodoMinModel> listaMin = new ArrayList<EcDoquiNodiNodoMinModel>();

			for (EcDoquiNodiNodoModel ogg : lista) {
				EcDoquiNodiNodoMinModel nodoMin = new EcDoquiNodiNodoMinModel(ogg.getDescrNodo(), ogg.getCodNodo());
				if (!listaMin.contains(nodoMin))
					listaMin.add(nodoMin);
			}

			return Response.status(200).entity(listaMin).build();

		} catch (Exception e) {
			risposta.setCodice(1);
			risposta.setMessaggio(e.getMessage());
			return Response.status(404).entity(risposta).build();
		}
	}

	@POST
	@Path("/poststruttura")
	public Response getEcDoquiNodiStruttura(TokenModel token) {

		RispostaModel risposta = new RispostaModel();
		EcDoquiModel ecDoqui = new EcDoquiModel();

		ecDoqui = ActaClientSrvUtils.getEcDoqui(token.getToken());

		Integer valido = ecDoqui.getValido();

		if (valido == 1) {
			risposta.setCodice(1);
			risposta.setMessaggio("Il token non e' valido");
			return Response.status(404).entity(risposta).build();
		}

		EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();
		EcDoquiNodiStrutturaDao ecDoquiNodiStrutturaDao = new EcDoquiNodiStrutturaDaoImpl();
		try {
			ecDoqui = ecDoquiDao.findAllByToken(token.getToken()).get(0);
			List<EcDoquiNodiStrutturaModel> lista = ecDoquiNodiStrutturaDao.findByIdDoqui(ecDoqui.getIdDoqui());
			GenericComparator.sortList(lista, "opzione", SortType.ASC);

			List<EcDoquiNodiStrutturaMinModel> listaMin = new ArrayList<EcDoquiNodiStrutturaMinModel>();

			for (EcDoquiNodiStrutturaModel li : lista) {
				EcDoquiNodiStrutturaMinModel struMin = new EcDoquiNodiStrutturaMinModel(li.getDescrStruttura(),
						li.getCodStruttura());
				if (!listaMin.contains(struMin))
					listaMin.add(struMin);
			}

			return Response.status(200).entity(listaMin).build();

		} catch (Exception e) {
			risposta.setCodice(1);
			risposta.setMessaggio(e.getMessage());
			return Response.status(404).entity(risposta).build();
		}
	}

	@POST
	@Path("/postecdoqui")
	public Response postEcDoqui(TokenModel token) {

		RispostaModel risposta = new RispostaModel();
		EcDoquiModel ecDoqui = new EcDoquiModel();
		EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();
		try {
			ecDoqui = ecDoquiDao.findAllByToken(token.getToken()).get(0);

			EcDoquiMinModel vista = new EcDoquiMinModel(ecDoqui.getCodDossier(), ecDoqui.getCodFisc(),
					ecDoqui.getTipoProc(), ecDoqui.getInOut());

			return Response.status(200).entity(vista).build();

		} catch (Exception e) {
			risposta.setCodice(1);
			risposta.setMessaggio(e.getMessage());
			return Response.status(404).entity(risposta).build();
		}
	}

	@POST
	@Path("/postprot")
	public Response postProt(TokenModel token) {

		RispostaModel risposta = new RispostaModel();
		EcDoquiModel ecDoqui = new EcDoquiModel();
		EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();
		try {
			ecDoqui = ecDoquiDao.findAllByToken(token.getToken()).get(0);
			String dbTable = ecDoqui.getKey2();

			Integer idDoqui = Integer.valueOf(ecDoqui.getKey3());
			List<EcDoquiIterMinProt> lista = new ArrayList<EcDoquiIterMinProt>();

			switch (dbTable) {
			case Constants.EC_AUA_ITER:
				EcAualterDao ecAuaIterDao = new EcAualterDaoImpl();
				EcAuaIterModel ecAua = (ecAuaIterDao.findByKey(idDoqui));
				List<EcAuaIterModel> listaAua = ecAuaIterDao.findAllByIdAuaAndOggettoNotNull(ecAua.getIdAua());
				for (EcAuaIterModel ec : listaAua) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();

			case Constants.EC_EALX_ITER:
				EcEalxIterDao ecEalxIterDao = new EcEalxIterDaoImpl();
				EcEalxIterModel ecEalx = ecEalxIterDao.findByKey(idDoqui);
				List<EcEalxIterModel> listaEalx = ecEalxIterDao
						.findAllByIdAutorizzAndOggettoNotNull(ecEalx.getIdAutorizz());
				for (EcEalxIterModel ec : listaEalx) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_ENERGIA_ITER:
				EcEnergiaIterDao ecEnergia = new EcEnergiaIterDaoImpl();
				EcEnergiaIterModel ecEnergiaModel = ecEnergia.findByKey(idDoqui);
				List<EcEnergiaIterModel> listaEnergia = ecEnergia
						.findAllByIdEnergiaAndOggettoNotNull(ecEnergiaModel.getIdEnergia());
				for (EcEnergiaIterModel ec : listaEnergia) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_AM_VCODISCITER:
				EcAmVcoDiscIterDao ecAmVcoDisc2Dao = new EcAmVcoDiscIterDaoImpl();
				EcAmVcoDiscIter ecAmVcoDisc2 = ecAmVcoDisc2Dao.findByKey(idDoqui);
				List<EcAmVcoDiscIter> listaVcoDisc2 = ecAmVcoDisc2Dao
						.findAllByIdDiscAndOggettoNotNull(ecAmVcoDisc2.getIdDisc());
				for (EcAmVcoDiscIter ec : listaVcoDisc2) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_AM_VCORECSITER:
				EcAmVcoRecsIterDao ecAmVcoRecsIterDao = new EcAmVcoRecsIterDaoImpl();
				EcAmVcoRecsIter ecAmVcoRecsIter = ecAmVcoRecsIterDao.findByKey(idDoqui);
				List<EcAmVcoRecsIter> listaVcoRecs2 = ecAmVcoRecsIterDao
						.findAllByIdRecsAndOggettoNotNull(ecAmVcoRecsIter.getIdRecs());
				for (EcAmVcoRecsIter ec : listaVcoRecs2) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_AM_VCOSMALITER:
				EcAmVcoSmalIterDao ecAmVcoSmalIterDao = new EcAmVcoSmalIterDaoImpl();
				EcAmVcoSmalIter ecAmVcoSmalIter = ecAmVcoSmalIterDao.findByKey(idDoqui);
				List<EcAmVcoSmalIter> listaVcoSmal2 = ecAmVcoSmalIterDao
						.findAllByIdSmalAndOggettoNotNull(ecAmVcoSmalIter.getIdSmal());
				for (EcAmVcoSmalIter ec : listaVcoSmal2) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_H2O_SCARPUBBFOGNITER:
				EcH2oScarPubbFognIterDao ecH2oScarPubbFognIterDao = new EcH2oScarPubbFognIterDaoImpl();
				EcH2oScarPubbFognIterModel ecH2oScarPubbFognIter = ecH2oScarPubbFognIterDao.findByKey(idDoqui);
				List<EcH2oScarPubbFognIterModel> listaPubbFogn3 = ecH2oScarPubbFognIterDao
						.findAllByIdScaricoAndOggettoNotNull(ecH2oScarPubbFognIter.getIdScarico());
				for (EcH2oScarPubbFognIterModel ec : listaPubbFogn3) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_H2O_SFIORATORITER:
				EcH2oSfioratoriIterDao ecH2oSfioratoriIterDao = new EcH2oSfioratoriIterDaoImpl();
				EcH2oSfioratoriIterModel ecH2oSfioratoriIter = ecH2oSfioratoriIterDao.findByKey(idDoqui);
				List<EcH2oSfioratoriIterModel> listaSfioratori2 = ecH2oSfioratoriIterDao
						.findAllByIdSfioratoreAndOggettoNotNull(ecH2oSfioratoriIter.getIdSfioratore());
				for (EcH2oSfioratoriIterModel ec : listaSfioratori2) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_H2O_METEORICHEITER:
				EcH2oMeteoricheIterDao meteoricheDao = new Ech2oMeteoricheDaoImpl();
				EcH2oMeteoricheIterModel meteoriche = meteoricheDao.findByKey(idDoqui);
				List<EcH2oMeteoricheIterModel> listaMeteoriche = meteoricheDao
						.findAllByIdMeteoricheAndOggettoNotNull(meteoriche.getIdMeteoriche());
				for (EcH2oMeteoricheIterModel ec : listaMeteoriche) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_H2O_SCARINDUSTRIALITER:
				EcH2oScarIndustrialiIterDao scarIndDao = new EcH2oScarIndustrialiIterDaoImpl();
				EcH2oScarIndustrialiIterModel scarInd = scarIndDao.findByKey(idDoqui);
				List<EcH2oScarIndustrialiIterModel> listaScarInd = scarIndDao
						.findAllByIdScarAndOggetoNotNull(scarInd.getIdScar());
				for (EcH2oScarIndustrialiIterModel ec : listaScarInd) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_IPPC_EVENTI:
				EcIppcEventiDao ecIppcEventiDao = new EcIppcEventiDaoImpl();
				EcIppcEventiModel ecIppcEventi = ecIppcEventiDao.findByKey(idDoqui);
				List<EcIppcEventiModel> listaEventi = ecIppcEventiDao
						.findAllByIdIterProcAndOggettoNotNull(ecIppcEventi.getIdIterProc());
				for (EcIppcEventiModel ec : listaEventi) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_IPPC_POSTAIA:
				EcIppcPostAiaDao ecIppcPostAiaDao = new EcIppcPostAiaDaoImpl();
				EcIppcPostAiaModel ecIppcPostaia = ecIppcPostAiaDao.findByKey(idDoqui);
				List<EcIppcPostAiaModel> listaPosta = ecIppcPostAiaDao
						.findAllByIdIterProcAndOggettoNotNull(ecIppcPostaia.getIdIterProc());
				for (EcIppcPostAiaModel ec : listaPosta) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_IPPC_FIDEJ:
				EcIppcFidejDao fidejDao = new EcIppcFidejDaoImpl();
				EcIppcFidejModel fidej = fidejDao.findByKey(idDoqui);
				List<EcIppcFidejModel> listaFidej = fidejDao
						.findAllByIdIterProcAndOggettoNotNull(fidej.getIdIterProc());
				for (EcIppcFidejModel ec : listaFidej) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			case Constants.EC_OM_ITER:
				EcOmIterDao omDao = new EcOmIterDaoImpl();
				EcOmIterModel omIter = omDao.findByKey(idDoqui);
				List<EcOmIterModel> listaOm = omDao.findAllByIdOliAndOggettoNotNull(omIter.getIdOli());
				for (EcOmIterModel ec : listaOm) {
					if (null != ec.getProt() && null != ec.getDataProt() && null != ec.getDirezione()
							&& null != ec.getOggetto()) {
						EcDoquiIterMinProt ealxMin = new EcDoquiIterMinProt(ec.getProt(), ec.getDataProt(),
								ec.getDirezione(), ec.getOggetto());
						if (!lista.contains(ealxMin))
							lista.add(ealxMin);
					}
				}
				return Response.status(200).entity(lista).build();
			default:
				risposta.setCodice(1);
				risposta.setMessaggio("il record (KEY2 + KEY3) non e' presente in tabella");
				return Response.status(404).entity(risposta).build();
			}

		} catch (Exception e) {
			risposta.setCodice(1);
			risposta.setMessaggio("il record (KEY2 - KEY3) non e' presente in tabella");
			return Response.status(404).entity(risposta).build();
		}
	}

	@GET
	@Path("/callRedirect/{t}")
	@Produces("application/json")
	public Response redirectUri(@PathParam(value = "t") String token) {
		URI locationOk = null;
		URI locationKO = null;

		EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();
		List<EcDoquiModel> listadoquiModels = ecDoquiDao.findAllByToken(token);

		if (listadoquiModels.size() > 0) {
			try {
				locationOk = new URI("/#/run?t=" + token);
			} catch (URISyntaxException e) {
				logger.error(e.getMessage(), e);
			}
			return Response.temporaryRedirect(locationOk).build();
		} else {
			try {
				locationOk = new URI("/#/tokenerror");
			} catch (URISyntaxException e) {
				logger.error(e.getMessage(), e);
			}
			return Response.temporaryRedirect(locationKO).build();
		}

	}

	@GET
	@Path("/currentUser")
	@Produces("application/json")
	public Response getUser(@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders,
			@Context HttpServletRequest request) {
		UserInfo userInfo = new BackendService().getCurrentUser(request);
		return Response.status(200).entity(userInfo).build();
	}

	@GET
	@Path("/localLogout")
	@Produces("application/json")
	public void logoutClearSession(@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders,
			@Context HttpServletRequest request) {
		new BackendService().localLogout(request);
	}

}