package it.csi.edsdoq.edsdoqweb.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import it.csi.edsdoq.edsdoqweb.dto.dao.EcDoquiDao;
import it.csi.edsdoq.edsdoqweb.dto.dao.impl.EcDoquiDaoImpl;
import it.csi.edsdoq.edsdoqweb.dto.model.EcDoquiModel;
import it.csi.edsdoq.edsdoqweb.util.Constants;

/**
 * @author franc
 *
 */
@WebServlet("/call")
public  class LoginRedirect extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private static final String REDIRECT_OK = "/#/run?t=#value#";
	private static final String REDIRECT_KO = "/#/tokenerror";
	
	Logger logger = Logger.getLogger(Constants.LOGGER_NAME);

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		
		String token = request.getParameter("t");
		// ricerca token nella tabella edsdoqui
		try {
			EcDoquiDao ecDoquiDao = new EcDoquiDaoImpl();
			List<EcDoquiModel> listadoquiModels = ecDoquiDao.findAllByToken(token);
			
			String newUrl = request.getContextPath();
//			String contextPath = request.getServletContext().getContextPath();
			
			if (listadoquiModels.size()>0) {
				String rerirectToken = newUrl+REDIRECT_OK.replaceAll("#value#", token);
				String nextJSP = rerirectToken;
				response.setContentType("text/html");
	
				response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
				response.setHeader("Location", nextJSP);  
			}
			else {
				response.setContentType("text/html");
	
				response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
				response.setHeader("Location", newUrl+REDIRECT_KO);  
			}
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
		}
		
//		if (listadoquiModels.size()>0)  {
//			String rerirectToken = newUrl+REDIRECT_OK.replaceAll("#value#", token);
//			String nextJSP = rerirectToken;
//			try {
//				//RequestDispatcher rd = getServletContext().getRequestDispatcher(nextJSP);
//				RequestDispatcher rd = getServletContext().getRequestDispatcher("/toRedirect.jsp");
//				request.getSession().setAttribute("t", token);
//				rd.forward(request, response);   
//				//request.getRequestDispatcher(nextJSP).include(request, response);
//			} catch (ServletException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			//response.sendRedirect(nextJSP);
//		} else {
//			response.sendRedirect(newUrl+REDIRECT_KO);
//		}

       
	}
	
}