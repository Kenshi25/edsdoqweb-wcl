/**
 * 
 */
package it.csi.edsdoq.edsdoqweb.servlet;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import it.csi.edsdoq.edsdoqweb.util.Constants;

/**
 * @author franc
 *
 */
@WebFilter(servletNames = "ShibbolethEmulator", urlPatterns = {"/shibbolethEmulator"})
public class ShibbolethEmulator implements javax.servlet.Filter  {

	Logger logger = Logger.getLogger(Constants.LOGGER_NAME);

	@Override
	public void destroy() {
	
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		
		MutableHttpServletRequest requestWrapper = new MutableHttpServletRequest(req);
		
		String shibIrideIdentitaDigitale = (String) request.getParameter("Shib-Iride-IdentitaDigitale");
		requestWrapper.addHeader("Shib-Iride-IdentitaDigitale", shibIrideIdentitaDigitale);
		requestWrapper.getHeaderNames();
		requestWrapper.getHeader("Shib-Iride-IdentitaDigitale")	;
		chain.doFilter(requestWrapper, response);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
				
	}

}
