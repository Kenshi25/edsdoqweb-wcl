/**
 * 
 */
package it.csi.edsdoq.edsdoqweb.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import it.csi.edsdoq.edsdoqweb.util.BackendService;
import it.csi.edsdoq.edsdoqweb.util.Constants;



/**
 * @author franc
 *
 */
@WebServlet("/localLogoutServlet")
public class LocalLogoutServlet extends HttpServlet {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -2111862920586239957L;
	
	Logger logger = Logger.getLogger(Constants.LOGGER_NAME);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
		logger.info("invalido la sessione prima di effettuare il logoff ");
		new BackendService().localLogout(req);
	}
	
	

}
